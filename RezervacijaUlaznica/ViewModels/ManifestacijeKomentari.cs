﻿using RezervacijaUlaznica.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.ViewModels
{
    public class ManifestacijeKomentari
    {

        public Manifestacija manifestacija { get; set; }
        public List<Komentar> komentari { get; set; }

        public ManifestacijeKomentari(Manifestacija manifestacija, List<Komentar> komentari)
        {
            this.manifestacija = manifestacija;
            this.komentari = komentari;
        }

        public ManifestacijeKomentari()
        {
        }
    }
}