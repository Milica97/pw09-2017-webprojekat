﻿using RezervacijaUlaznica.Models;
using RezervacijaUlaznica.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RezervacijaUlaznica.Controllers
{
    public class ProdavacController : Controller
    {
        // GET: Prodavac
        public ActionResult Index()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            return View(prodavac);
        }

        public ActionResult NovaManifestacija()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Manifestacije", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Kreiraj(string naziv, string tip, string brojMijestaRegular,string brojMijestaVip,string brojMijestaFanPit, string datumIVrijemeOdrzavanja, string cijenaRegularKarte,string ulica, string broj, string grad, string postanskiBroj)
        {
           Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            int br1 = 0;
            long postanskiBroj1 = 0;
            int brojMijesta1 = 0;
            int brojMijestaRegular1 = 0;
            int brojMijestaFanPit1 = 0;
            int brojMijestaVip1 = 0;
            int cijenaRegularKarte1 = 0;
            DateTime datumIVrijemeOdrzavanja1 = DateTime.Now;
            TipManifestacije tip1 = TipManifestacije.BIOSKOP;

            if (!broj.Equals(""))
            {

                br1 = int.Parse(broj);
            }
            if (!postanskiBroj.Equals(""))
            {
                postanskiBroj1 = long.Parse(postanskiBroj);
            }
            if (!brojMijestaRegular.Equals(""))
            {
                brojMijestaRegular1 = int.Parse(brojMijestaRegular);
            }
            if (!brojMijestaFanPit.Equals(""))
            {
                brojMijestaFanPit1 = int.Parse(brojMijestaFanPit);
            }
            if (!brojMijestaVip.Equals(""))
            {
                brojMijestaVip1 = int.Parse(brojMijestaVip);
            }

            brojMijesta1 = brojMijestaFanPit1 + brojMijestaRegular1 + brojMijestaVip1;

            if (!cijenaRegularKarte.Equals(""))
            {
                cijenaRegularKarte1 = int.Parse(cijenaRegularKarte);
            }
            if (!datumIVrijemeOdrzavanja.Equals(""))
            {
                datumIVrijemeOdrzavanja1 = DateTime.Parse(datumIVrijemeOdrzavanja);
            }
            if (!tip.Equals(""))
            {
                tip1 = (TipManifestacije)Enum.Parse(typeof(TipManifestacije), tip);
            }

            try
            {
                int a = int.Parse(ulica);
                TempData["Message"] = $"Polje 'Ulica' ne  moze biti broj!";
                return View("NovaManifestacija");
            }
            catch
            {
               //nista
            }
            try
            {
                int a = int.Parse(grad);
                TempData["Message"] = $"Polje 'Grad' ne  moze biti broj!";
                return View("NovaManifestacija");
            }
            catch
            {
                //nista
            }

            if (naziv.Equals("") && brojMijesta1 == 0 && datumIVrijemeOdrzavanja.Equals("") && cijenaRegularKarte1 == 0 && ulica.Equals("") && br1 == 0 && grad.Equals("") && postanskiBroj1 == 0)
            {
                TempData["Message"] = $"Sva polja moraju biti popunjena!";
                return View("NovaManifestacija");
            }

            if (naziv.Equals(""))
            {
                TempData["Message"] = $"Polje 'Naziv' mora biti popunjeno !";
                return View("NovaManifestacija");
            }
            else if (brojMijestaRegular1 == 0)
            {
                TempData["Message"] = $"Polje 'Broj REGULAR mijesta' mora biti popunjeno!";
                return View("NovaManifestacija");
            }
            else if (brojMijestaRegular1 < 0)
            {
                TempData["Message"] = $"Polje 'Broj REGULAR mijesta' ne moze biti manje od 0!";
                return View("NovaManifestacija");
            }
            else if (brojMijestaVip1 < 0)
            {
                TempData["Message"] = $"Polje 'Broj VIP mijesta' ne moze biti manje od 0!";
                return View("NovaManifestacija");
            }
            else if (brojMijestaFanPit1 < 0)
            {
                TempData["Message"] = $"Polje 'Broj FAN PIT mijesta' ne moze biti manje od 0!";
                return View("NovaManifestacija");
            }
            else if (datumIVrijemeOdrzavanja.Equals(""))
            {
                TempData["message"] = $"Polje 'Datum i vrijeme' mora biti popunjeno!";
                return View("NovaManifestacija");
            }
            else if (cijenaRegularKarte1 == 0)
            {
                TempData["message"] = $"Polje 'Cijena REGULAR karte' mora biti popunjeno!";
                return View("NovaManifestacija");

            }else if (cijenaRegularKarte1 < 0)
            {
                TempData["Message"] = $"Polje 'Cijena REGULAR karte' ne moze biti manje od 0!";
                return View("NovaManifestacija");
            }
            else if (ulica.Equals(""))
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Ulica' mora biti popunjeno!";
                return View("NovaManifestacija");
            }
            else if (br1 == 0)
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Broj' mora biti popunjeno!";
                return View("NovaManifestacija");
            }
            else if (br1 < 0)
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Broj' ne moze biti negativan broj!";
                return View("NovaManifestacija");
            }
            else if (grad.Equals(""))
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Grad' mora biti popunjeno!";
                return View("NovaManifestacija");
            }
            else if (postanskiBroj1 == 0)
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Postanski broj' mora biti popunjeno!";
                return View("NovaManifestacija");
                
            }
            else if (postanskiBroj1 < 0)
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Postanski broj' ne moze biti negativan broj!";
                return View("NovaManifestacija");

            }

            string relativePath = "";
            if (Request.Files["img"].ContentLength>0)
            {
                relativePath = "~/Images/" + Path.GetFileName(Request.Files["img"].FileName);
                string fp = Server.MapPath(relativePath);

                Request.Files["img"].SaveAs(fp);
            }
            else
            {
                relativePath = " ";
            }

            MjestoOdrzavanja mjesto = new MjestoOdrzavanja(ulica, br1, grad, postanskiBroj1);
            Manifestacija novaManifestacija = new Manifestacija(manifestacije.Count + 1, naziv, tip1, brojMijestaRegular1, brojMijestaVip1, brojMijestaFanPit1, brojMijesta1, datumIVrijemeOdrzavanja1, cijenaRegularKarte1, StatusManifestacije.NEAKTIVNO, relativePath, mjesto, false, prodavac.Id, -1, brojMijestaRegular1, brojMijestaVip1, brojMijestaFanPit1);

            foreach (Manifestacija m in manifestacije)
            {
                string[] adresa = m.Mjesto.Adresa.Split(',');
                string[] tokens = adresa[0].Split(' ');
                string u = tokens[0];
                int br = int.Parse(tokens[1]);
                string gr = adresa[1];
                long posBr = long.Parse(adresa[2]);

                if (u.ToLower() == ulica.ToLower() && br == br1 && gr.ToLower() == grad.ToLower() && posBr == postanskiBroj1 && m.DatumIVrijemeOdrzavanja == novaManifestacija.DatumIVrijemeOdrzavanja)
                {
                    TempData["Message"] = $"Vec se odrzava manifestacija na tom mjestu u to vrijeme";
                    return RedirectToAction("NovaManifestacija");
                }

            }

            if (datumIVrijemeOdrzavanja1 < DateTime.Now)
            {
                TempData["Message"] = $"Datum i vrijeme manifestacije ne moze biti u proslosti!";
                return RedirectToAction("NovaManifestacija");
            }
            
            manifestacije.Add(novaManifestacija);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.SacuvajManifestacije(novaManifestacija);
            return RedirectToAction("Manifestacije");
        }

        public ActionResult Manifestacije()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Manifestacije", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacijeProdavca = new List<Manifestacija>();

            foreach (var m in manifestacije)
            {
                if (m.DatumIVrijemeOdrzavanja < DateTime.Now && !m.Status.ToString().Equals("OTKAZANO") && !m.Status.ToString().Equals("NEAKTIVNO") && !m.Status.ToString().Equals("ZAVRSENO"))
                {
                    m.Zavrsena = true;
                    m.Status = StatusManifestacije.ZAVRSENO;

                }
            }

            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja < DateTime.Now && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    r.karta.ZaKojuManifestaciju.Zavrsena = true;
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            foreach (var m in manifestacije)
            {
                if (m.IdProdavca == prodavac.Id)
                {
                    manifestacijeProdavca.Add(m);
                }
            }

            return View(manifestacijeProdavca);
        }

        public ActionResult Detalji(int id)
        {
            //id manifestacije

            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);

                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }

            Manifestacija pomocna = new Manifestacija();
            ManifestacijeKomentari mankom = new ManifestacijeKomentari();
            List<Komentar> pomkomentari = new List<Komentar>();

            foreach (var m in manifestacije)
            {
                if (m.DatumIVrijemeOdrzavanja < DateTime.Now && !m.Status.ToString().Equals("OTKAZANO") && !m.Status.ToString().Equals("NEAKTIVNO") && !m.Status.ToString().Equals("ZAVRSENO"))
                {
                    m.Zavrsena = true;
                    m.Status = StatusManifestacije.ZAVRSENO;

                }
            }
            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja < DateTime.Now && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    r.karta.ZaKojuManifestaciju.Zavrsena = true;
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    if (m.Zavrsena == false)
                    {
                        mankom = new ManifestacijeKomentari(m, null);
                        return View(mankom);
                    }
                    else
                    {
                        foreach (var kom in pomocna1)
                        {
                            if (kom.Manifestacija.Id == id && kom.Tip.ToString().Equals("ODOBREN"))
                            {
                                pomkomentari.Add(kom);
                            }
                        }

                        mankom = new ManifestacijeKomentari(m, pomkomentari);
                        return View(mankom);

                    }
                }
            }
            //foreach (var m in manifestacije)
            //{
            //    if (m.Id == id)
            //    {
            //        return View(m);
            //    }
            //}

            return RedirectToAction("Manifestacije");
        }

        public ActionResult AzurirajManifestaciju(int id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    if (m.Zavrsena == true)
                    {
                        TempData["azuriranje"] = $"Nije moguce azurirati manifestaciju jer je zavrsena";
                        return RedirectToAction("Manifestacije");
                    }
                    else
                    {
                        return View(m);
                    }
                }
            }
            return RedirectToAction("Detalji");

        }

        [HttpPost]
        public ActionResult Azuriraj(string naziv, string tip, string brojMijestaRegular, string brojMijestaVip, string brojMijestaFanPit, string datumIVrijemeOdrzavanja, string cijenaRegularKarte, string ulica, string broj, string grad, string postanskiBroj, int id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];

            int br1 = 0;
            long postanskiBroj1 = 0;
            int brojMijesta1 = 0;
            int brojMijestaRegular1 = 0;
            int brojMijestaVip1 = 0;
            int brojMijestaFanPit1 = 0;
            int cijenaRegularKarte1 = 0;
            DateTime datumIVrijemeOdrzavanja1 = DateTime.Now;
            TipManifestacije tip1 = TipManifestacije.BIOSKOP;

            if (!broj.Equals(""))
            {
                br1 = int.Parse(broj);
            }
            if (!postanskiBroj.Equals(""))
            {
                postanskiBroj1 = long.Parse(postanskiBroj);
            }
            if (!brojMijestaRegular.Equals(""))
            {
                brojMijestaRegular1 = int.Parse(brojMijestaRegular);
            }
            if (!brojMijestaFanPit.Equals(""))
            {
                brojMijestaFanPit1 = int.Parse(brojMijestaFanPit);
            }
            if (!brojMijestaVip.Equals(""))
            {
                brojMijestaVip1 = int.Parse(brojMijestaVip);
            }

            brojMijesta1 = brojMijestaFanPit1 + brojMijestaRegular1 + brojMijestaVip1;

            if (!cijenaRegularKarte.Equals(""))
            {
                cijenaRegularKarte1 = int.Parse(cijenaRegularKarte);
            }
            if (!datumIVrijemeOdrzavanja.Equals(""))
            {
                try
                {
                    datumIVrijemeOdrzavanja1 = DateTime.Parse(datumIVrijemeOdrzavanja);
                }
                catch
                {
                    TempData["Message"] = $"Polje 'Datum i vrijeme odrzavanja' nije u dobrom formatu!";
                    foreach (var m in manifestacije)
                    {
                        if (m.Id == id)
                        {
                            return View("AzurirajManifestaciju", m);
                        }
                    }
                }
            }
            if (!tip.Equals(""))
            {
                tip1 = (TipManifestacije)Enum.Parse(typeof(TipManifestacije), tip);
            }
           

            if (naziv.Equals("") && brojMijesta1 == 0 && datumIVrijemeOdrzavanja.Equals("") && cijenaRegularKarte1 == 0 && ulica.Equals("") && br1 == 0 && grad.Equals("") && postanskiBroj1 == 0)
            {
                TempData["Message"] = $"Sva polja moraju biti popunjena!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }

            if (naziv.Equals(""))
            {
                TempData["Message"] = $"Polje 'Naziv' mora biti popunjeno !";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (brojMijestaRegular1 == 0)
            {
                TempData["Message"] = $"Polje 'Broj REGULAR mijesta' mora biti popunjeno!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (brojMijestaRegular1 < 0)
            {
                TempData["Message"] = $"Polje 'Broj REGULAR mijesta' ne moze biti manje od 0!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (brojMijestaVip1 < 0)
            {
                TempData["Message"] = $"Polje 'Broj VIP mijesta' ne moze biti manje od 0!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (brojMijestaFanPit1 < 0)
            {
                TempData["Message"] = $"Polje 'Broj FAN PIT mijesta' ne moze biti manje od 0!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (datumIVrijemeOdrzavanja.Equals(""))
            {
                TempData["message"] = $"Polje 'Datum i vrijeme' mora biti popunjeno!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (cijenaRegularKarte1 == 0)
            {
                TempData["message"] = $"Polje 'Cijena REGULAR karte' mora biti popunjeno!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (cijenaRegularKarte1 < 0)
            {
                TempData["message"] = $"Polje 'Cijena REGULAR karte' ne moze biti manje od 0!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (ulica.Equals(""))
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Ulica' mora biti popunjeno!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (br1 == 0)
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Broj' mora biti popunjeno!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (br1 < 0)
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Broj' ne moze biti negativan broj!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (grad.Equals(""))
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Grad' mora biti popunjeno!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (postanskiBroj1 == 0)
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Postanski broj' mora biti popunjeno!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            else if (postanskiBroj1 < 0)
            {
                TempData["message"] = $"Polje 'Mjesto odrzavanja - Postanski broj' ne moze biti negativan broj!";
                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }

            if (datumIVrijemeOdrzavanja1 < DateTime.Now)
            {
                TempData["Message"] = $"Datum i vrijeme manifestacije ne moze biti u proslosti!";

                foreach (var m in manifestacije)
                {
                    if (m.Id == id)
                    {
                        return View("AzurirajManifestaciju", m);
                    }
                }
            }
            
            string relativePath ="";
            if (Request.Files["img"].ContentLength > 0)
            {
                relativePath = "~/Images/" + Path.GetFileName(Request.Files["img"].FileName);
                string physicalPath = Server.MapPath(relativePath);

                Request.Files["img"].SaveAs(physicalPath);
            }
            

            MjestoOdrzavanja mjesto = new MjestoOdrzavanja(ulica, br1, grad, postanskiBroj1);
            Manifestacija nova = new Manifestacija(id, naziv, tip1, brojMijestaRegular1,brojMijestaVip1,brojMijestaFanPit1, brojMijesta1, datumIVrijemeOdrzavanja1, cijenaRegularKarte1, StatusManifestacije.NEAKTIVNO, relativePath, mjesto, false, prodavac.Id, -1,10,10,10);

            foreach (Manifestacija m in manifestacije)
            {
                if (m.Id != id)
                {
                    string[] adresa = m.Mjesto.Adresa.Split(',');
                    string[] tokens = adresa[0].Split(' ');
                    string u = tokens[0];
                    int br = int.Parse(tokens[1]);
                    string gr = adresa[1];
                    long posBr = long.Parse(adresa[2]);

                    if (u.ToLower() == ulica.ToLower() && br == br1 && gr.ToLower() == grad.ToLower() && posBr == postanskiBroj1 && m.DatumIVrijemeOdrzavanja == nova.DatumIVrijemeOdrzavanja)
                    {
                        TempData["Message"] = $"Vec se odrzava manifestacija na tom mjestu u to vrijeme";
                        if (m.Id == id)
                        {
                            return View("AzurirajManifestaciju", m);
                        }
                    }
                }

            }

            foreach (var m in manifestacije)
            {
                if (m.Id == nova.Id)
                {
                    if (brojMijestaRegular1 < m.BrojMijestaRegular)
                    {
                        TempData["Message"] = $"Broj REGULAR mijesta ne moze biti manji nego sto je bio";
                        return View("AzurirajManifestaciju", m);
                    }
                    if (brojMijestaVip1 < m.BrojMijestaVip)
                    {
                        TempData["Message"] = $"Broj VIP mijesta ne moze biti manji nego sto je bio";
                        return View("AzurirajManifestaciju", m);

                    }

                    if (brojMijestaFanPit1 < m.BrojMijestaFanPit)
                    {
                        TempData["Message"] = $"Broj FAN PIT mijesta ne moze biti manji nego sto je bio";
                        return View("AzurirajManifestaciju", m);

                    }

                    m.Naziv = nova.Naziv;
                    m.Tip = nova.Tip;
                    m.BrSlobodnihRegular += nova.BrojMijestaRegular - m.BrojMijestaRegular;
                    m.BrSlobodnihVip += nova.BrojMijestaVip - m.BrojMijestaVip;
                    m.BrSlobodnihFanPit += nova.BrojMijestaFanPit - m.BrojMijestaFanPit;
                    m.BrojMijestaRegular = nova.BrojMijestaRegular;
                    m.BrojMijestaVip = nova.BrojMijestaVip;
                    m.BrojMijestaFanPit = nova.BrojMijestaFanPit;
                    m.BrojMijesta = nova.BrojMijesta;
                    m.DatumIVrijemeOdrzavanja = nova.DatumIVrijemeOdrzavanja;
                    m.CijenaRegularKarte = nova.CijenaRegularKarte;
                    if (nova.Poster.Equals(""))
                    {
                        m.Poster = m.Poster;
                    }
                    else
                    {
                        m.Poster = nova.Poster;
                    }
                    m.Mjesto.Adresa = nova.Mjesto.Adresa;
                    m.Mjesto.Ulica = nova.Mjesto.Ulica;
                    m.Mjesto.Broj = nova.Mjesto.Broj;
                    m.Mjesto.Grad = nova.Mjesto.Grad;
                    m.Mjesto.PostanskiBroj = nova.Mjesto.PostanskiBroj;
                    break;
                }
            }

            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.Id == nova.Id)
                {
                    r.karta.ZaKojuManifestaciju.Naziv = nova.Naziv;
                    r.karta.ZaKojuManifestaciju.Tip = nova.Tip;
                    r.karta.ZaKojuManifestaciju.BrojMijesta = nova.BrojMijesta;
                    r.karta.ZaKojuManifestaciju.BrSlobodnihRegular += nova.BrojMijestaRegular - r.karta.ZaKojuManifestaciju.BrojMijestaRegular;
                    r.karta.ZaKojuManifestaciju.BrSlobodnihVip += nova.BrojMijestaVip - r.karta.ZaKojuManifestaciju.BrojMijestaVip;
                    r.karta.ZaKojuManifestaciju.BrSlobodnihFanPit += nova.BrojMijestaFanPit - r.karta.ZaKojuManifestaciju.BrojMijestaFanPit;
                    r.karta.ZaKojuManifestaciju.BrojMijestaRegular = nova.BrojMijestaRegular;
                    r.karta.ZaKojuManifestaciju.BrojMijestaVip = nova.BrojMijestaVip;
                    r.karta.ZaKojuManifestaciju.BrojMijestaFanPit = nova.BrojMijestaFanPit;
                    r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja = nova.DatumIVrijemeOdrzavanja;
                    r.karta.DatumIvrijemeManifestacije = nova.DatumIVrijemeOdrzavanja;
                    r.karta.ZaKojuManifestaciju.CijenaRegularKarte = nova.CijenaRegularKarte;
                    r.karta.ZaKojuManifestaciju.Poster = nova.Poster;
                    r.karta.ZaKojuManifestaciju.Mjesto.Adresa = nova.Mjesto.Adresa;
                }
            }
            HttpContext.Application["rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);

            return RedirectToAction("Manifestacije");
        }

        public ActionResult Profil()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Profil", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Administrator");
                }
            }
            return View(prodavac);
        }

        public ActionResult AzurirajSvojePodatke(int id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            Korisnik k = new Korisnik();
            foreach (var p in prodavci)
            {
                if (p.Id == id)
                {
                    return View(p);
                }
            }
            return View(k);

        }

        [HttpPost]
        public ActionResult AzurirajProdavca(Korisnik azuriranProdavac)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            

            if (!ModelState.IsValid)
            {
                TempData["greska"] = $"Sva polja moraju biti popunjena!";
                return View("AzurirajSvojePodatke", prodavac);

            }

            if (kupci.Contains(azuriranProdavac) || admini.Contains(azuriranProdavac))
            {
                TempData["greska"] = $"Korisnik sa korisnickim imenom: {azuriranProdavac.KorisnickoIme} vec postoji!";
                return View("AzurirajSvojePodatke", prodavac);
            }
            foreach (var k in prodavci)
            {
                if (k.Id != azuriranProdavac.Id)
                {
                    if (k.KorisnickoIme.Equals(azuriranProdavac.KorisnickoIme))
                    {
                        TempData["greska"] = $"Korisnik sa korisnickim imenom: {azuriranProdavac.KorisnickoIme} vec postoji!";
                        return View("AzurirajSvojePodatke", prodavac);
                    }
                }
            }

            foreach (Korisnik k in prodavci)
            {
                if (k.Id == azuriranProdavac.Id)
                {
                    k.KorisnickoIme = azuriranProdavac.KorisnickoIme;
                    k.Lozinka = azuriranProdavac.Lozinka;
                    k.Ime = azuriranProdavac.Ime;
                    k.Prezime = azuriranProdavac.Prezime;
                    k.Pol = azuriranProdavac.Pol;
                    k.DatumRodjenja = azuriranProdavac.DatumRodjenja;
                    break;
                }
            }

            HttpContext.Application["prodavci"] = prodavci;
            Podaci.AzururajProdavca(prodavci);
            return RedirectToAction("Profil");
        }

        public ActionResult Karte()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("SveKarte", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("Karte", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];

            List<Rezervacija> pomocna = new List<Rezervacija>();

            foreach (var m in manifestacije)
            {
                if (m.DatumIVrijemeOdrzavanja < DateTime.Now && !m.Status.ToString().Equals("OTKAZANO") && !m.Status.ToString().Equals("NEAKTIVNO") && !m.Status.ToString().Equals("ZAVRSENO"))
                {
                    m.Zavrsena = true;
                    m.Status = StatusManifestacije.ZAVRSENO;

                }
            }
            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja < DateTime.Now && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    r.karta.ZaKojuManifestaciju.Zavrsena = true;
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            foreach (var rez in rezervacije)
            {
                if (rez.karta.ZaKojuManifestaciju.IdProdavca == prodavac.Id && rez.karta.Status.ToString().Equals("REZERVISANA"))
                {
                    pomocna.Add(rez);
                }
            }
            return View(pomocna);
        }

        public ActionResult OdobriKomentare()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("SveKarte", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("Karte", "Administrator");
                }
            }
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            List<Komentar> pomocna = new List<Komentar>();
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);
                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }
            foreach (var k in pomocna1)
            {
                if (k.Tip.ToString().Equals("CEKANJE") && k.Manifestacija.IdProdavca == prodavac.Id)
                {
                    pomocna.Add(k);
                }
            }
            return View(pomocna);
        }

        public ActionResult Odobri(string id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna = new List<Komentar>();
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);
                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }
            foreach (var k in pomocna1)
            {
                if (k.Id.ToString() == id)
                {
                    k.Tip = TipKomentara.ODOBREN;
                    break;
                }
            }
            foreach (var kom in komentari)
            {
                if (kom.Id.ToString() == id)
                {
                    kom.Tip = TipKomentara.ODOBREN;
                    break;
                }
            }

            HttpContext.Application["komentari"] = komentari;
            Podaci.AzurirajKomentare(pomocna1);

            return RedirectToAction("Komentari");
        }

        public ActionResult Odbaci(string id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna = new List<Komentar>();
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);
                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }
            foreach (var k in pomocna1)
            {
                if (k.Id.ToString() == id)
                {
                    k.Tip = TipKomentara.ODBIJEN;
                    break;
                }
            }
            foreach (var kom in komentari)
            {
                if (kom.Id.ToString() == id)
                {
                    kom.Tip = TipKomentara.ODBIJEN;
                    break;
                }
            }

            HttpContext.Application["komentari"] = komentari;
            Podaci.AzurirajKomentare(pomocna1);
            return RedirectToAction("Komentari");
        }

        public ActionResult Komentari()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("SveKarte", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("Karte", "Administrator");
                }
            }

            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna = new List<Komentar>();
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);
                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }
            foreach (var k in pomocna1)
            {
                if (k.Manifestacija.IdProdavca == prodavac.Id)
                {
                    pomocna.Add(k);
                }
            }

            return View(pomocna);
        }
        
        public ActionResult Odjava()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (prodavac == null)
            {
                if (admin == null)
                {
                    if (kupac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Kupac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            Session["prodavci"] = null;
            return RedirectToAction("Index", "Home");
        }
        
    }

}