﻿using RezervacijaUlaznica.Models;
using RezervacijaUlaznica.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RezervacijaUlaznica.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> sortiraneManifestacije = new List<Manifestacija>();

            if (kupac != null)
            {
                ViewBag.ulogovan = $"Vec ste ulogovani kao kupac!";
            }
            else if (admin != null)
            {
                ViewBag.ulogovan = $"Vec ste ulogovani kao administrator!";
            }
            else if (prodavac != null)
            {
                ViewBag.ulogovan = $"Vec ste ulogovani kao prodavac!";
            }

            foreach (var m in manifestacije)
            {
                if (m.DatumIVrijemeOdrzavanja < DateTime.Now && !m.Status.ToString().Equals("OTKAZANO") && !m.Status.ToString().Equals("NEAKTIVNO") && !m.Status.ToString().Equals("ZAVRSENO"))
                {
                    m.Zavrsena = true;
                    m.Status = StatusManifestacije.ZAVRSENO;

                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja < DateTime.Now && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    r.karta.ZaKojuManifestaciju.Zavrsena = true;
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);
            sortiraneManifestacije = manifestacije.OrderBy(m => m.DatumIVrijemeOdrzavanja).ToList();

            return View(sortiraneManifestacije);
        }

        [HttpPost]
        public ActionResult LogIn(string korisnickoIme, string lozinka)
        {
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            Korisnik user = kupci.Find(u => u.KorisnickoIme.Equals(korisnickoIme) && u.Lozinka.Equals(lozinka));

            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            Korisnik admin = admini.Find(u => u.KorisnickoIme.Equals(korisnickoIme) && u.Lozinka.Equals(lozinka));

            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            Korisnik prodavac = prodavci.Find(u => u.KorisnickoIme.Equals(korisnickoIme) && u.Lozinka.Equals(lozinka));

            if (user == null)
            {
                if (prodavac == null)
                {
                    if (admin == null)
                    {
                        TempData["message"] = $"Korisnik sa unijetim kredencijalima ne postoji!Provjerite unijete podatke ili se registrujte!";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        Session["admin"] = admin;
                        return RedirectToAction("NoveManifestacije", "Administrator");//Stranica za admina
                    }
                }
                else
                {
                    Session["prodavci"] = prodavac;
                    return RedirectToAction("Index", "Prodavac");//Stranica za prodavca gdje vidi svoj profil
                }
            }
            if (user.Obrisan == true)
            {
                TempData["message"] = "Korisnik je obrisan iz sistema";
                return RedirectToAction("Index");
            }
            Session["kupci"] = user;
            return RedirectToAction("Index", "Kupac"); //stranica za kupca

        }

        public ActionResult Detalji(int id)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);

                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }

            Manifestacija pomocna = new Manifestacija();
            ManifestacijeKomentari mankom = new ManifestacijeKomentari();
            List<Komentar> pomkomentari = new List<Komentar>();

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    if (m.Zavrsena == false)
                    {
                        mankom = new ManifestacijeKomentari(m, null);
                        return View(mankom);
                    }
                    else
                    {
                        foreach (var kom in pomocna1)
                        {
                            if (kom.Manifestacija.Id == id && kom.Tip.ToString().Equals("ODOBREN"))
                            {
                                pomkomentari.Add(kom);
                            }
                        }

                        mankom = new ManifestacijeKomentari(m, pomkomentari);
                        return View(mankom);

                    }
                }
            }
            return RedirectToAction("Index");

        }
        public ActionResult DetaljiPocetna(int id)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);

                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }

            Manifestacija pomocna = new Manifestacija();
            ManifestacijeKomentari mankom = new ManifestacijeKomentari();
            List<Komentar> pomkomentari = new List<Komentar>();

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    if (m.Zavrsena == false)
                    {
                        mankom = new ManifestacijeKomentari(m, null);
                        return View(mankom);
                    }
                    else
                    {
                        foreach (var kom in pomocna1)
                        {
                            if (kom.Manifestacija.Id == id && kom.Tip.ToString().Equals("ODOBREN"))
                            {
                                pomkomentari.Add(kom);
                            }
                        }

                        mankom = new ManifestacijeKomentari(m, pomkomentari);
                        return View(mankom);

                    }
                }
            }
            return RedirectToAction("Index");
        }
        public ActionResult Pretrazi(string naziv, string mjesto, string datumOd, string datumDo, string lokacija, string cijenaOd, string cijenaDo)
        {
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> pretraga = (List<Manifestacija>)HttpContext.Application["pretraga"];

            List<Manifestacija> pretrazene = new List<Manifestacija>();
            List<Manifestacija> pomocnaNaziv = new List<Manifestacija>();
            List<Manifestacija> pomocnaMjesto = new List<Manifestacija>();
            List<Manifestacija> pomocnaObaDatuma = new List<Manifestacija>();
            List<Manifestacija> pomocnaCijene = new List<Manifestacija>();
            List<Manifestacija> pomocna4 = new List<Manifestacija>();
            List<Manifestacija> pomocnaLokacija = new List<Manifestacija>();
            List<Manifestacija> pomocna6 = new List<Manifestacija>();
            List<string> gradovi = new List<string>();

            if (naziv == null && mjesto == null && datumOd == null && datumDo == null && lokacija == null && cijenaOd == null && cijenaDo == null)
            {
                return View(pretraga);
            }

            double zbirOcjena = 0;
            double brOcjena = 0;

            foreach (var m in manifestacije)
            {
                foreach (var k in komentari)
                {
                    if (k.IdManifestacije == m.Id)
                    {
                        if (k.Tip.ToString().Equals("ODOBREN"))
                        {
                            zbirOcjena += k.Ocjena;
                            brOcjena += 1;
                        }
                    }
                }

                try
                {
                    m.ProsjecnaOcjena = zbirOcjena / brOcjena;
                    if (m.ProsjecnaOcjena.ToString() == "NaN")
                    {
                        m.ProsjecnaOcjena = -1;
                    }
                    zbirOcjena = 0;
                    brOcjena = 0;
                }
                catch
                {
                    m.ProsjecnaOcjena = -1;
                }
            }
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            foreach (var r in rezervacije)
            {
                foreach (var m in manifestacije)
                {
                    if (m.Id == r.karta.ZaKojuManifestaciju.Id)
                    {
                        r.karta.ZaKojuManifestaciju.ProsjecnaOcjena = m.ProsjecnaOcjena;
                    }
                }
            }
            HttpContext.Application["rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);

            string ulicaiBroj = "";
            string grad = "";

            string ulica = "";
            int broj = 0;

            try
            {
                string[] mjestoOdrzavanja = mjesto.Split(',');
                ulicaiBroj = mjestoOdrzavanja[0];
                grad = mjestoOdrzavanja[1];
                if (grad.StartsWith(" "))
                {
                    grad = grad.Substring(1, grad.Length - 1);
                }
                string[] ub = ulicaiBroj.Split(' ');
                ulica = ub[0];
                broj = int.Parse(ub[1]);
            }
            catch
            {
                if (mjesto.Equals(""))
                {
                    ulica = "";
                    broj = 0;
                }
                else
                {
                    TempData["pretraga"] = $"Polje 'Mjesto odrzavanja' mora biti popunjeno u formatu 'ULICA BROJ,GRAD'";
                    return RedirectToAction("Index");
                }

            }

            DateTime dateOd = DateTime.Now;
            DateTime dateDo = DateTime.Now;

            if (!datumOd.Equals("") && !datumDo.Equals(""))
            {
                try
                {
                    dateOd = DateTime.Parse(datumOd);
                    dateDo = DateTime.Parse(datumDo);
                }
                catch
                {
                    TempData["pretraga"] = $"Polje 'Datum odrzavanja - godina' nije dobro popunjeno!";
                    return RedirectToAction("Index");
                }
            }
            else
            {
                if (!datumOd.Equals(""))
                {
                    try
                    {
                        dateOd = DateTime.Parse(datumOd);
                    }
                    catch
                    {
                        TempData["pretraga"] = $"Polje 'Datum odrzavanja od - godina' nije dobro popunjeno!";
                        return RedirectToAction("Index");
                    }

                }
                else
                {
                    if (!datumDo.Equals(""))
                    {
                        try
                        {
                            dateDo = DateTime.Parse(datumDo);
                        }
                        catch
                        {
                            TempData["pretraga"] = $"Polje 'Datum odrzavanja do - godina' nije dobro popunjeno!";
                            return RedirectToAction("Index");
                        }

                    }
                }
            }

            double cijena1 = 0;
            double cijena2 = 0;


            if (!naziv.Equals("") && !mjesto.Equals("") && !datumOd.Equals("") && !datumDo.Equals("") && !lokacija.Equals("") && !cijenaOd.Equals("") && !cijenaDo.Equals(""))
            {
                dateOd = DateTime.Parse(datumOd);
                dateDo = DateTime.Parse(datumDo);
                cijena1 = double.Parse(cijenaOd);
                cijena2 = double.Parse(cijenaDo);

                foreach (var m in manifestacije)
                {
                    if (m.Naziv.ToUpper().Equals(naziv.ToUpper()) && m.Mjesto.Ulica.ToUpper() == ulica.ToUpper() && m.Mjesto.Broj == broj && m.Mjesto.Grad.ToUpper() == grad.ToUpper() && m.DatumIVrijemeOdrzavanja >= dateOd && m.DatumIVrijemeOdrzavanja <= dateDo && m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()) && m.CijenaRegularKarte >= cijena1 && m.CijenaRegularKarte <= cijena2)
                    {
                        pretrazene.Add(m);
                    }
                }

                HttpContext.Application["pretraga"] = pretrazene;
                return View(pretrazene);
            }
            else
            {
                if (!naziv.Equals(""))
                {
                    foreach (Manifestacija m in manifestacije)
                    {
                        if (m.Naziv.ToUpper() == naziv.ToUpper())
                        {
                            pomocnaNaziv.Add(m);
                        }
                    }
                }
                else
                {

                    if (!mjesto.Equals(""))
                    {
                        foreach (Manifestacija m in manifestacije)
                        {
                            if (m.Mjesto.Ulica.ToUpper() == ulica.ToUpper() && m.Mjesto.Broj == broj && m.Mjesto.Grad.ToUpper() == grad.ToUpper())
                            {
                                pomocnaMjesto.Add(m);
                            }
                        }
                    }
                    else
                    {
                        if (!datumOd.Equals("") && !datumDo.Equals(""))
                        {
                            dateOd = DateTime.Parse(datumOd);
                            dateDo = DateTime.Parse(datumDo);

                            foreach (Manifestacija m in manifestacije)
                            {
                                if (m.DatumIVrijemeOdrzavanja > dateOd && m.DatumIVrijemeOdrzavanja < dateDo)
                                {
                                    pomocnaObaDatuma.Add(m);
                                }
                            }
                        }
                        else
                        {
                            if (!datumOd.Equals(""))
                            {
                                dateOd = DateTime.Parse(datumOd);
                                foreach (Manifestacija m in manifestacije)
                                {
                                    if (m.DatumIVrijemeOdrzavanja > dateOd)
                                    {
                                        pomocnaObaDatuma.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!datumDo.Equals(""))
                                {
                                    dateDo = DateTime.Parse(datumDo);
                                    foreach (Manifestacija m in manifestacije)
                                    {
                                        if (m.DatumIVrijemeOdrzavanja < dateDo)
                                        {
                                            pomocnaObaDatuma.Add(m);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                                    {
                                        cijena1 = double.Parse(cijenaOd);
                                        cijena2 = double.Parse(cijenaDo);

                                        foreach (Manifestacija m in manifestacije)
                                        {
                                            if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                                            {
                                                pomocnaCijene.Add(m);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!cijenaOd.Equals(""))
                                        {
                                            cijena1 = double.Parse(cijenaOd);

                                            foreach (Manifestacija m in manifestacije)
                                            {
                                                if (m.CijenaRegularKarte > cijena1)
                                                {
                                                    pomocnaCijene.Add(m);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (!cijenaDo.Equals(""))
                                            {
                                                cijena2 = double.Parse(cijenaDo);

                                                foreach (Manifestacija m in manifestacije)
                                                {
                                                    if (m.CijenaRegularKarte < cijena2)
                                                    {
                                                        pomocnaCijene.Add(m);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (!lokacija.Equals(""))
                                                {
                                                    foreach (Manifestacija m in manifestacije)
                                                    {
                                                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                        {
                                                            pomocnaLokacija.Add(m);
                                                        }
                                                    }
                                                    pretrazene = pomocnaLokacija;
                                                }
                                                else
                                                {
                                                    pretrazene = manifestacije;
                                                }
                                                HttpContext.Application["pretraga"] = pretrazene;
                                                return View(pretrazene);

                                            }

                                            if (!lokacija.Equals(""))
                                            {
                                                foreach (Manifestacija m in pomocnaCijene)
                                                {
                                                    if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                    {
                                                        pomocnaLokacija.Add(m);
                                                    }
                                                }
                                                pretrazene = pomocnaLokacija;
                                            }
                                            else
                                            {
                                                pretrazene = pomocnaCijene;
                                            }

                                            HttpContext.Application["pretraga"] = pretrazene;
                                            return View(pretrazene);
                                        }

                                        if (!lokacija.Equals(""))
                                        {
                                            foreach (Manifestacija m in pomocnaCijene)
                                            {
                                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                {
                                                    pomocnaLokacija.Add(m);
                                                }
                                            }
                                            pretrazene = pomocnaLokacija;
                                        }
                                        else
                                        {
                                            pretrazene = pomocnaCijene;
                                        }

                                        HttpContext.Application["pretraga"] = pretrazene;
                                        return View(pretrazene);

                                    }
                                }

                                if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);
                                    cijena2 = double.Parse(cijenaDo);

                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                                        {
                                            pomocnaCijene.Add(m);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!cijenaOd.Equals(""))
                                    {
                                        cijena1 = double.Parse(cijenaOd);

                                        foreach (Manifestacija m in pomocnaObaDatuma)
                                        {
                                            if (m.CijenaRegularKarte > cijena1)
                                            {
                                                pomocnaCijene.Add(m);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!cijenaDo.Equals(""))
                                        {
                                            cijena2 = double.Parse(cijenaDo);

                                            foreach (Manifestacija m in pomocnaObaDatuma)
                                            {
                                                if (m.CijenaRegularKarte < cijena2)
                                                {
                                                    pomocnaCijene.Add(m);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (!lokacija.Equals(""))
                                            {
                                                foreach (Manifestacija m in pomocnaObaDatuma)
                                                {
                                                    if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                    {
                                                        pomocnaLokacija.Add(m);
                                                    }
                                                }
                                                pretrazene = pomocnaLokacija;
                                            }
                                            else
                                            {
                                                pretrazene = pomocnaObaDatuma;
                                            }
                                            HttpContext.Application["pretraga"] = pretrazene;
                                            return View(pretrazene);

                                        }

                                        if (!lokacija.Equals(""))
                                        {
                                            foreach (Manifestacija m in pomocnaObaDatuma)
                                            {
                                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                {
                                                    pomocnaLokacija.Add(m);
                                                }
                                            }
                                            pretrazene = pomocnaLokacija;
                                        }
                                        else
                                        {
                                            pretrazene = pomocnaCijene;
                                        }

                                        HttpContext.Application["pretraga"] = pretrazene;
                                        return View(pretrazene);
                                    }

                                    if (!lokacija.Equals(""))
                                    {
                                        foreach (Manifestacija m in pomocnaCijene)
                                        {
                                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                            {
                                                pomocnaLokacija.Add(m);
                                            }
                                        }
                                        pretrazene = pomocnaLokacija;
                                    }
                                    else
                                    {
                                        pretrazene = pomocnaCijene;
                                    }

                                    HttpContext.Application["pretraga"] = pretrazene;
                                    return View(pretrazene);

                                }
                            }

                            if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);
                                cijena2 = double.Parse(cijenaDo);

                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                                    {
                                        pomocnaCijene.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!cijenaOd.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);

                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.CijenaRegularKarte > cijena1)
                                        {
                                            pomocnaCijene.Add(m);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!cijenaDo.Equals(""))
                                    {
                                        cijena2 = double.Parse(cijenaDo);

                                        foreach (Manifestacija m in pomocnaObaDatuma)
                                        {
                                            if (m.CijenaRegularKarte < cijena2)
                                            {
                                                pomocnaCijene.Add(m);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!lokacija.Equals(""))
                                        {
                                            foreach (Manifestacija m in pomocnaObaDatuma)
                                            {
                                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                {
                                                    pomocnaLokacija.Add(m);
                                                }
                                            }
                                            pretrazene = pomocnaLokacija;
                                        }
                                        else
                                        {
                                            pretrazene = pomocnaObaDatuma;
                                        }
                                        HttpContext.Application["pretraga"] = pretrazene;
                                        return View(pretrazene);

                                    }

                                    if (!lokacija.Equals(""))
                                    {
                                        foreach (Manifestacija m in pomocnaObaDatuma)
                                        {
                                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                            {
                                                pomocnaLokacija.Add(m);
                                            }
                                        }
                                        pretrazene = pomocnaLokacija;
                                    }
                                    else
                                    {
                                        pretrazene = pomocnaCijene;
                                    }

                                    HttpContext.Application["pretraga"] = pretrazene;
                                    return View(pretrazene);
                                }

                                if (!lokacija.Equals(""))
                                {
                                    foreach (Manifestacija m in pomocnaCijene)
                                    {
                                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                        {
                                            pomocnaLokacija.Add(m);
                                        }
                                    }
                                    pretrazene = pomocnaLokacija;
                                }
                                else
                                {
                                    pretrazene = pomocnaCijene;
                                }

                                HttpContext.Application["pretraga"] = pretrazene;
                                return View(pretrazene);

                            }


                        }


                    }



                }

                if (!mjesto.Equals(""))
                {
                    foreach (Manifestacija m in pomocnaNaziv)
                    {
                        if (m.Mjesto.Ulica.ToUpper() == ulica.ToUpper() && m.Mjesto.Broj == broj && m.Mjesto.Grad.ToUpper() == grad.ToUpper())
                        {
                            pomocnaMjesto.Add(m);
                        }
                    }
                }
                else
                {
                    if (!datumOd.Equals("") && !datumDo.Equals(""))
                    {
                        dateOd = DateTime.Parse(datumOd);
                        dateDo = DateTime.Parse(datumDo);

                        foreach (Manifestacija m in pomocnaNaziv)
                        {
                            if (m.DatumIVrijemeOdrzavanja > dateOd && m.DatumIVrijemeOdrzavanja < dateDo)
                            {
                                pomocnaObaDatuma.Add(m);
                            }
                        }
                    }
                    else
                    {
                        if (!datumOd.Equals(""))
                        {
                            dateOd = DateTime.Parse(datumOd);
                            foreach (Manifestacija m in pomocnaNaziv)
                            {
                                if (m.DatumIVrijemeOdrzavanja > dateOd)
                                {
                                    pomocnaObaDatuma.Add(m);
                                }
                            }
                        }
                        else
                        {
                            if (!datumDo.Equals(""))
                            {
                                dateDo = DateTime.Parse(datumDo);
                                foreach (Manifestacija m in pomocnaNaziv)
                                {
                                    if (m.DatumIVrijemeOdrzavanja < dateDo)
                                    {
                                        pomocnaObaDatuma.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);
                                    cijena2 = double.Parse(cijenaDo);

                                    foreach (Manifestacija m in pomocnaNaziv)
                                    {
                                        if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                                        {
                                            pomocnaCijene.Add(m);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!cijenaOd.Equals(""))
                                    {
                                        cijena1 = double.Parse(cijenaOd);

                                        foreach (Manifestacija m in pomocnaNaziv)
                                        {
                                            if (m.CijenaRegularKarte > cijena1)
                                            {
                                                pomocnaCijene.Add(m);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!cijenaDo.Equals(""))
                                        {
                                            cijena2 = double.Parse(cijenaDo);

                                            foreach (Manifestacija m in pomocnaNaziv)
                                            {
                                                if (m.CijenaRegularKarte < cijena2)
                                                {
                                                    pomocnaCijene.Add(m);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (!lokacija.Equals(""))
                                            {
                                                foreach (Manifestacija m in pomocnaNaziv)
                                                {
                                                    if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                    {
                                                        pomocnaLokacija.Add(m);
                                                    }
                                                }
                                                pretrazene = pomocnaLokacija;
                                            }
                                            else
                                            {
                                                pretrazene = pomocnaNaziv;
                                            }
                                            HttpContext.Application["pretraga"] = pretrazene;
                                            return View(pretrazene);

                                        }

                                        if (!lokacija.Equals(""))
                                        {
                                            foreach (Manifestacija m in pomocnaCijene)
                                            {
                                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                {
                                                    pomocnaLokacija.Add(m);
                                                }
                                            }
                                            pretrazene = pomocnaLokacija;
                                        }
                                        else
                                        {
                                            pretrazene = pomocnaCijene;
                                        }

                                        HttpContext.Application["pretraga"] = pretrazene;
                                        return View(pretrazene);
                                    }

                                    if (!lokacija.Equals(""))
                                    {
                                        foreach (Manifestacija m in pomocnaCijene)
                                        {
                                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                            {
                                                pomocnaLokacija.Add(m);
                                            }
                                        }
                                        pretrazene = pomocnaLokacija;
                                    }
                                    else
                                    {
                                        pretrazene = pomocnaCijene;
                                    }

                                    HttpContext.Application["pretraga"] = pretrazene;
                                    return View(pretrazene);

                                }
                            }

                            if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);
                                cijena2 = double.Parse(cijenaDo);

                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                                    {
                                        pomocnaCijene.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!cijenaOd.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);

                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.CijenaRegularKarte > cijena1)
                                        {
                                            pomocnaCijene.Add(m);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!cijenaDo.Equals(""))
                                    {
                                        cijena2 = double.Parse(cijenaDo);

                                        foreach (Manifestacija m in pomocnaObaDatuma)
                                        {
                                            if (m.CijenaRegularKarte < cijena2)
                                            {
                                                pomocnaCijene.Add(m);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!lokacija.Equals(""))
                                        {
                                            foreach (Manifestacija m in pomocnaObaDatuma)
                                            {
                                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                {
                                                    pomocnaLokacija.Add(m);
                                                }
                                            }
                                            pretrazene = pomocnaLokacija;
                                        }
                                        else
                                        {
                                            pretrazene = pomocnaObaDatuma;
                                        }
                                        HttpContext.Application["pretraga"] = pretrazene;
                                        return View(pretrazene);

                                    }

                                    if (!lokacija.Equals(""))
                                    {
                                        foreach (Manifestacija m in pomocnaObaDatuma)
                                        {
                                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                            {
                                                pomocnaLokacija.Add(m);
                                            }
                                        }
                                        pretrazene = pomocnaLokacija;
                                    }
                                    else
                                    {
                                        pretrazene = pomocnaCijene;
                                    }

                                    HttpContext.Application["pretraga"] = pretrazene;
                                    return View(pretrazene);
                                }

                                if (!lokacija.Equals(""))
                                {
                                    foreach (Manifestacija m in pomocnaCijene)
                                    {
                                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                        {
                                            pomocnaLokacija.Add(m);
                                        }
                                    }
                                    pretrazene = pomocnaLokacija;
                                }
                                else
                                {
                                    pretrazene = pomocnaCijene;
                                }

                                HttpContext.Application["pretraga"] = pretrazene;
                                return View(pretrazene);

                            }
                        }

                        if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                        {
                            cijena1 = double.Parse(cijenaOd);
                            cijena2 = double.Parse(cijenaDo);

                            foreach (Manifestacija m in pomocnaObaDatuma)
                            {
                                if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                                {
                                    pomocnaCijene.Add(m);
                                }
                            }
                        }
                        else
                        {
                            if (!cijenaOd.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);

                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.CijenaRegularKarte > cijena1)
                                    {
                                        pomocnaCijene.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!cijenaDo.Equals(""))
                                {
                                    cijena2 = double.Parse(cijenaDo);

                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.CijenaRegularKarte < cijena2)
                                        {
                                            pomocnaCijene.Add(m);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!lokacija.Equals(""))
                                    {
                                        foreach (Manifestacija m in pomocnaObaDatuma)
                                        {
                                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                            {
                                                pomocnaLokacija.Add(m);
                                            }
                                        }
                                        pretrazene = pomocnaLokacija;
                                    }
                                    else
                                    {
                                        pretrazene = pomocnaObaDatuma;
                                    }
                                    HttpContext.Application["pretraga"] = pretrazene;
                                    return View(pretrazene);

                                }

                                if (!lokacija.Equals(""))
                                {
                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                        {
                                            pomocnaLokacija.Add(m);
                                        }
                                    }
                                    pretrazene = pomocnaLokacija;
                                }
                                else
                                {
                                    pretrazene = pomocnaCijene;
                                }

                                HttpContext.Application["pretraga"] = pretrazene;
                                return View(pretrazene);
                            }

                            if (!lokacija.Equals(""))
                            {
                                foreach (Manifestacija m in pomocnaCijene)
                                {
                                    if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                    {
                                        pomocnaLokacija.Add(m);
                                    }
                                }
                                pretrazene = pomocnaLokacija;
                            }
                            else
                            {
                                pretrazene = pomocnaCijene;
                            }

                            HttpContext.Application["pretraga"] = pretrazene;
                            return View(pretrazene);

                        }


                    }
                    if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                    {
                        cijena1 = double.Parse(cijenaOd);
                        cijena2 = double.Parse(cijenaDo);

                        foreach (Manifestacija m in pomocnaObaDatuma)
                        {
                            if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                            {
                                pomocnaCijene.Add(m);
                            }
                        }
                    }
                    else
                    {
                        if (!cijenaOd.Equals(""))
                        {
                            cijena1 = double.Parse(cijenaOd);

                            foreach (Manifestacija m in pomocnaObaDatuma)
                            {
                                if (m.CijenaRegularKarte > cijena1)
                                {
                                    pomocnaCijene.Add(m);
                                }
                            }
                        }
                        else
                        {
                            if (!cijenaDo.Equals(""))
                            {
                                cijena2 = double.Parse(cijenaDo);

                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.CijenaRegularKarte < cijena2)
                                    {
                                        pomocnaCijene.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!lokacija.Equals(""))
                                {
                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                        {
                                            pomocnaLokacija.Add(m);
                                        }
                                    }
                                    pretrazene = pomocnaLokacija;
                                }
                                else
                                {
                                    pretrazene = pomocnaObaDatuma;
                                }
                                HttpContext.Application["pretraga"] = pretrazene;
                                return View(pretrazene);

                            }

                            if (!lokacija.Equals(""))
                            {
                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                    {
                                        pomocnaLokacija.Add(m);
                                    }
                                }
                                pretrazene = pomocnaLokacija;
                            }
                            else
                            {
                                pretrazene = pomocnaCijene;
                            }

                            HttpContext.Application["pretraga"] = pretrazene;
                            return View(pretrazene);
                        }

                        if (!lokacija.Equals(""))
                        {
                            foreach (Manifestacija m in pomocnaCijene)
                            {
                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                {
                                    pomocnaLokacija.Add(m);
                                }
                            }
                            pretrazene = pomocnaLokacija;
                        }
                        else
                        {
                            pretrazene = pomocnaCijene;
                        }

                        HttpContext.Application["pretraga"] = pretrazene;
                        return View(pretrazene);

                    }

                    if (!lokacija.Equals(""))
                    {
                        foreach (Manifestacija m in pomocnaCijene)
                        {
                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                            {
                                pomocnaLokacija.Add(m);
                            }
                        }
                        pretrazene = pomocnaLokacija;
                    }
                    else
                    {
                        pretrazene = pomocnaCijene;

                    }


                }


                if (!datumOd.Equals("") && !datumDo.Equals(""))
                {
                    dateOd = DateTime.Parse(datumOd);
                    dateDo = DateTime.Parse(datumDo);

                    foreach (Manifestacija m in pomocnaMjesto)
                    {
                        if (m.DatumIVrijemeOdrzavanja > dateOd && m.DatumIVrijemeOdrzavanja < dateDo)
                        {
                            pomocnaObaDatuma.Add(m);
                        }
                    }
                }
                else
                {
                    if (!datumOd.Equals(""))
                    {
                        dateOd = DateTime.Parse(datumOd);
                        foreach (Manifestacija m in pomocnaMjesto)
                        {
                            if (m.DatumIVrijemeOdrzavanja > dateOd)
                            {
                                pomocnaObaDatuma.Add(m);
                            }
                        }
                    }
                    else
                    {
                        if (!datumDo.Equals(""))
                        {
                            dateDo = DateTime.Parse(datumDo);
                            foreach (Manifestacija m in pomocnaMjesto)
                            {
                                if (m.DatumIVrijemeOdrzavanja < dateDo)
                                {
                                    pomocnaObaDatuma.Add(m);
                                }
                            }
                        }
                        else
                        {
                            if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);
                                cijena2 = double.Parse(cijenaDo);

                                foreach (Manifestacija m in pomocnaMjesto)
                                {
                                    if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                                    {
                                        pomocnaCijene.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!cijenaOd.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);

                                    foreach (Manifestacija m in pomocnaMjesto)
                                    {
                                        if (m.CijenaRegularKarte > cijena1)
                                        {
                                            pomocnaCijene.Add(m);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!cijenaDo.Equals(""))
                                    {
                                        cijena2 = double.Parse(cijenaDo);

                                        foreach (Manifestacija m in pomocnaMjesto)
                                        {
                                            if (m.CijenaRegularKarte < cijena2)
                                            {
                                                pomocnaCijene.Add(m);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!lokacija.Equals(""))
                                        {
                                            foreach (Manifestacija m in pomocnaMjesto)
                                            {
                                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                                {
                                                    pomocnaLokacija.Add(m);
                                                }
                                            }
                                            pretrazene = pomocnaLokacija;
                                        }
                                        else
                                        {
                                            pretrazene = pomocnaMjesto;
                                        }
                                        HttpContext.Application["pretraga"] = pretrazene;
                                        return View(pretrazene);

                                    }

                                    if (!lokacija.Equals(""))
                                    {
                                        foreach (Manifestacija m in pomocnaMjesto)
                                        {
                                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                            {
                                                pomocnaLokacija.Add(m);
                                            }
                                        }
                                        pretrazene = pomocnaLokacija;
                                    }
                                    else
                                    {
                                        pretrazene = pomocnaMjesto;
                                    }

                                    HttpContext.Application["pretraga"] = pretrazene;
                                    return View(pretrazene);
                                }

                                if (!lokacija.Equals(""))
                                {
                                    foreach (Manifestacija m in pomocnaCijene)
                                    {
                                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                        {
                                            pomocnaLokacija.Add(m);
                                        }
                                    }
                                    pretrazene = pomocnaLokacija;
                                }
                                else
                                {
                                    pretrazene = pomocnaCijene;
                                }

                                HttpContext.Application["pretraga"] = pretrazene;
                                return View(pretrazene);

                            }
                        }

                        if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                        {
                            cijena1 = double.Parse(cijenaOd);
                            cijena2 = double.Parse(cijenaDo);

                            foreach (Manifestacija m in pomocnaObaDatuma)
                            {
                                if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                                {
                                    pomocnaCijene.Add(m);
                                }
                            }
                        }
                        else
                        {
                            if (!cijenaOd.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);

                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.CijenaRegularKarte > cijena1)
                                    {
                                        pomocnaCijene.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!cijenaDo.Equals(""))
                                {
                                    cijena2 = double.Parse(cijenaDo);

                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.CijenaRegularKarte < cijena2)
                                        {
                                            pomocnaCijene.Add(m);
                                        }
                                    }
                                }
                                else
                                {
                                    if (!lokacija.Equals(""))
                                    {
                                        foreach (Manifestacija m in pomocnaObaDatuma)
                                        {
                                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                            {
                                                pomocnaLokacija.Add(m);
                                            }
                                        }
                                        pretrazene = pomocnaLokacija;
                                    }
                                    else
                                    {
                                        pretrazene = pomocnaObaDatuma;
                                    }
                                    HttpContext.Application["pretraga"] = pretrazene;
                                    return View(pretrazene);

                                }

                                if (!lokacija.Equals(""))
                                {
                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                        {
                                            pomocnaLokacija.Add(m);
                                        }
                                    }
                                    pretrazene = pomocnaLokacija;
                                }
                                else
                                {
                                    pretrazene = pomocnaCijene;
                                }

                                HttpContext.Application["pretraga"] = pretrazene;
                                return View(pretrazene);
                            }

                            if (!lokacija.Equals(""))
                            {
                                foreach (Manifestacija m in pomocnaCijene)
                                {
                                    if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                    {
                                        pomocnaLokacija.Add(m);
                                    }
                                }
                                pretrazene = pomocnaLokacija;
                            }
                            else
                            {
                                pretrazene = pomocnaCijene;
                            }

                            HttpContext.Application["pretraga"] = pretrazene;
                            return View(pretrazene);

                        }
                    }

                    if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                    {
                        cijena1 = double.Parse(cijenaOd);
                        cijena2 = double.Parse(cijenaDo);

                        foreach (Manifestacija m in pomocnaObaDatuma)
                        {
                            if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                            {
                                pomocnaCijene.Add(m);
                            }
                        }
                    }
                    else
                    {
                        if (!cijenaOd.Equals(""))
                        {
                            cijena1 = double.Parse(cijenaOd);

                            foreach (Manifestacija m in pomocnaObaDatuma)
                            {
                                if (m.CijenaRegularKarte > cijena1)
                                {
                                    pomocnaCijene.Add(m);
                                }
                            }
                        }
                        else
                        {
                            if (!cijenaDo.Equals(""))
                            {
                                cijena2 = double.Parse(cijenaDo);

                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.CijenaRegularKarte < cijena2)
                                    {
                                        pomocnaCijene.Add(m);
                                    }
                                }
                            }
                            else
                            {
                                if (!lokacija.Equals(""))
                                {
                                    foreach (Manifestacija m in pomocnaObaDatuma)
                                    {
                                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                        {
                                            pomocnaLokacija.Add(m);
                                        }
                                    }
                                    pretrazene = pomocnaLokacija;
                                }
                                else
                                {
                                    pretrazene = pomocnaObaDatuma;
                                }
                                HttpContext.Application["pretraga"] = pretrazene;
                                return View(pretrazene);

                            }

                            if (!lokacija.Equals(""))
                            {
                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                    {
                                        pomocnaLokacija.Add(m);
                                    }
                                }
                                pretrazene = pomocnaLokacija;
                            }
                            else
                            {
                                pretrazene = pomocnaCijene;
                            }

                            HttpContext.Application["pretraga"] = pretrazene;
                            return View(pretrazene);
                        }

                        if (!lokacija.Equals(""))
                        {
                            foreach (Manifestacija m in pomocnaCijene)
                            {
                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                {
                                    pomocnaLokacija.Add(m);
                                }
                            }
                            pretrazene = pomocnaLokacija;
                        }
                        else
                        {
                            pretrazene = pomocnaCijene;
                        }

                        HttpContext.Application["pretraga"] = pretrazene;
                        return View(pretrazene);

                    }


                }

                if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
                {
                    cijena1 = double.Parse(cijenaOd);
                    cijena2 = double.Parse(cijenaDo);

                    foreach (Manifestacija m in pomocnaObaDatuma)
                    {
                        if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
                        {
                            pomocnaCijene.Add(m);
                        }
                    }
                }
                else
                {
                    if (!cijenaOd.Equals(""))
                    {
                        cijena1 = double.Parse(cijenaOd);

                        foreach (Manifestacija m in pomocnaObaDatuma)
                        {
                            if (m.CijenaRegularKarte > cijena1)
                            {
                                pomocnaCijene.Add(m);
                            }
                        }
                    }
                    else
                    {
                        if (!cijenaDo.Equals(""))
                        {
                            cijena2 = double.Parse(cijenaDo);

                            foreach (Manifestacija m in pomocnaObaDatuma)
                            {
                                if (m.CijenaRegularKarte < cijena2)
                                {
                                    pomocnaCijene.Add(m);
                                }
                            }
                        }
                        else
                        {
                            if (!lokacija.Equals(""))
                            {
                                foreach (Manifestacija m in pomocnaObaDatuma)
                                {
                                    if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                    {
                                        pomocnaLokacija.Add(m);
                                    }
                                }
                                pretrazene = pomocnaLokacija;
                            }
                            else
                            {
                                pretrazene = pomocnaObaDatuma;
                            }
                            HttpContext.Application["pretraga"] = pretrazene;
                            return View(pretrazene);

                        }

                        if (!lokacija.Equals(""))
                        {
                            foreach (Manifestacija m in pomocnaObaDatuma)
                            {
                                if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                                {
                                    pomocnaLokacija.Add(m);
                                }
                            }
                            pretrazene = pomocnaLokacija;
                        }
                        else
                        {
                            pretrazene = pomocnaCijene;
                        }

                        HttpContext.Application["pretraga"] = pretrazene;
                        return View(pretrazene);
                    }

                    if (!lokacija.Equals(""))
                    {
                        foreach (Manifestacija m in pomocnaCijene)
                        {
                            if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                            {
                                pomocnaLokacija.Add(m);
                            }
                        }
                        pretrazene = pomocnaLokacija;
                    }
                    else
                    {
                        pretrazene = pomocnaCijene;
                    }

                    HttpContext.Application["pretraga"] = pretrazene;
                    return View(pretrazene);

                }

                if (!lokacija.Equals(""))
                {
                    foreach (Manifestacija m in pomocnaCijene)
                    {
                        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
                        {
                            pomocnaLokacija.Add(m);
                        }
                    }
                    pretrazene = pomocnaLokacija;
                }
                else
                {
                    pretrazene = pomocnaCijene;

                }

                HttpContext.Application["pretraga"] = pretrazene;
                return View(pretrazene);

            }

            #region DobraPretraga


            //if (!naziv.Equals(""))
            //{
            //    foreach (Manifestacija m in manifestacije)
            //    {
            //        if (m.Naziv.ToUpper() == naziv.ToUpper())
            //        {
            //            pomocnaNaziv.Add(m);
            //        }
            //    }
            //}
            //else
            //{
            //    pomocnaNaziv = manifestacije;
            //}


            //if (!mjesto.Equals(""))
            //{
            //    foreach (Manifestacija m in pomocnaNaziv)
            //    {
            //        if (m.Mjesto.Ulica.ToUpper() == ulica.ToUpper() && m.Mjesto.Broj == broj && m.Mjesto.Grad.ToUpper() == grad.ToUpper())
            //        {
            //            pomocnaMjesto.Add(m);
            //        }
            //    }
            //}
            //else
            //{
            //    pomocnaMjesto = pomocnaNaziv;
            //}

            //if (!datumOd.Equals("") && !datumDo.Equals(""))
            //{
            //    dateOd = DateTime.Parse(datumOd);
            //    dateDo = DateTime.Parse(datumDo);

            //    foreach (Manifestacija m in pomocnaMjesto)
            //    {
            //        if (m.DatumIVrijemeOdrzavanja > dateOd && m.DatumIVrijemeOdrzavanja < dateDo)
            //        {
            //            pomocnaObaDatuma.Add(m);
            //        }
            //    }
            //    pomocna6 = pomocnaObaDatuma;
            //}
            //else
            //{
            //    if (!datumOd.Equals(""))
            //    {
            //        dateOd = DateTime.Parse(datumOd);
            //        foreach (Manifestacija m in pomocnaMjesto)
            //        {
            //            if (m.DatumIVrijemeOdrzavanja > dateOd)
            //            {
            //                pomocnaObaDatuma.Add(m);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        pomocnaObaDatuma = pomocnaMjesto;
            //    }

            //    if (!datumDo.Equals(""))
            //    {
            //        dateDo = DateTime.Parse(datumDo);
            //        foreach (Manifestacija m in pomocnaObaDatuma)
            //        {
            //            if (m.DatumIVrijemeOdrzavanja < dateDo)
            //            {
            //                pomocna6.Add(m);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        pomocna6 = pomocnaObaDatuma;
            //    }
            //}

            //if (!cijenaDo.Equals("") && !cijenaOd.Equals(""))
            //{
            //    cijena1 = double.Parse(cijenaOd);
            //    cijena2 = double.Parse(cijenaDo);

            //    foreach (Manifestacija m in pomocna6)
            //    {
            //        if (m.CijenaRegularKarte > cijena1 && m.CijenaRegularKarte < cijena2)
            //        {
            //            pomocnaCijene.Add(m);
            //        }
            //    }
            //    pomocna4 = pomocnaCijene;
            //}
            //else
            //{
            //    if (!cijenaOd.Equals(""))
            //    {
            //        cijena1 = double.Parse(cijenaOd);

            //        foreach (Manifestacija m in pomocna6)
            //        {
            //            if (m.CijenaRegularKarte > cijena1)
            //            {
            //                pomocnaCijene.Add(m);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        pomocnaCijene = pomocna6;
            //    }

            //    if (!cijenaDo.Equals(""))
            //    {
            //        cijena2 = double.Parse(cijenaDo);

            //        foreach (Manifestacija m in pomocnaCijene)
            //        {
            //            if (m.CijenaRegularKarte < cijena2)
            //            {
            //                pomocna4.Add(m);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        pomocna4 = pomocnaCijene;
            //    }

            //}

            //if (!lokacija.Equals(""))
            //{
            //    foreach (Manifestacija m in pomocna4)
            //    {
            //        if (m.Mjesto.Grad.ToUpper().Equals(lokacija.ToUpper()))
            //        {
            //            pomocnaLokacija.Add(m);
            //        }
            //    }
            //}
            //else
            //{
            //    pomocnaLokacija = pomocna4;
            //}

            //foreach (var m in pomocnaLokacija)
            //{
            //    if (!pretrazene.Contains(m))
            //    {
            //        pretrazene.Add(m);
            //    }
            //}


            //HttpContext.Application["pretraga"] = pretrazene;
            //return View(pretrazene);
            #endregion
        }
        public ActionResult Sortiraj(string sortirajPo, string opras)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> pretrazene = (List<Manifestacija>)HttpContext.Application["pretraga"];
            List<Manifestacija> sortirane = new List<Manifestacija>();

            if (opras.Equals(""))
            {
                opras = "Rastuce";
            }





            switch (sortirajPo)
            {
                case "Naziv":
                    if (opras.Equals("Rastuce"))
                    {
                        sortirane = pretrazene.OrderBy(v => v.Naziv).ToList();
                        break;
                    }
                    else
                    {
                        sortirane = pretrazene.OrderByDescending(v => v.Naziv).ToList();
                        break;
                    }

                case "Datum_odrzavanja":
                    if (opras.Equals("Rastuce"))
                    {
                        sortirane = pretrazene.OrderBy(v => v.DatumIVrijemeOdrzavanja).ToList();
                        break;
                    }
                    else
                    {
                        sortirane = pretrazene.OrderByDescending(v => v.DatumIVrijemeOdrzavanja).ToList();
                        break;
                    }
                case "Cijena_karte":
                    if (opras.Equals("Rastuce"))
                    {
                        sortirane = pretrazene.OrderBy(v => v.CijenaRegularKarte).ToList();
                        break;
                    }
                    else
                    {
                        sortirane = pretrazene.OrderByDescending(v => v.CijenaRegularKarte).ToList();
                        break;
                    }
                case "Mjesto_odrzavanja":
                    if (opras.Equals("Rastuce"))
                    {
                        sortirane = pretrazene.OrderBy(v => v.Mjesto.Adresa).ToList();
                        break;
                    }
                    else
                    {
                        sortirane = pretrazene.OrderByDescending(v => v.Mjesto.Adresa).ToList();
                        break;
                    }
                default:
                    sortirane = pretrazene;
                    break;
            }

            return View("Pretrazi", sortirane);
        }

        public ActionResult Filtriraj(string koncert, string festival, string pozoriste, string bioskop, string nerasprodate)
        {
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Manifestacija> pretrazene = (List<Manifestacija>)HttpContext.Application["pretraga"];

            List<Manifestacija> filtrirane = new List<Manifestacija>();

            if (koncert == null && bioskop == null && pozoriste == null && festival == null && nerasprodate == null)
            {
                filtrirane = pretrazene;
                return View("Pretrazi", filtrirane);
            }


            if (koncert != null && bioskop != null && pozoriste != null && festival != null && nerasprodate != null)
            {
                foreach (var m in pretrazene)
                    if (!m.Status.ToString().Equals("RASPRODATO"))
                    {
                        filtrirane.Add(m);
                    }

                return View("Pretrazi", filtrirane);
            }
            else
            {
                if (nerasprodate != null)
                {
                    if (festival != null)
                    {
                        foreach (var m in pretrazene)
                        {
                            if (m.Tip.ToString().Equals("FESTIVAL") && !m.Status.ToString().Equals("RASPRODATO"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }
                    
                    if (bioskop != null)
                    {
                        foreach (var m in pretrazene)
                        {
                            if (m.Tip.ToString().Equals("BIOSKOP") && !m.Status.ToString().Equals("RASPRODATO"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }

                    if (pozoriste != null)
                    {
                        foreach (var m in pretrazene)
                        {
                            if (m.Tip.ToString().Equals("POZORISTE") && !m.Status.ToString().Equals("RASPRODATO"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }


                    if (koncert != null)
                    {
                        foreach (var m in pretrazene)
                        {
                            if (m.Tip.ToString().Equals("KONCERT") && !m.Status.ToString().Equals("RASPRODATO"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }

                    if (koncert==null && festival==null && pozoriste==null && bioskop==null)
                    {
                        foreach(var m in pretrazene)
                        {
                            if (!m.Status.ToString().Equals("RASPRODATO"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }
                   
                    return View("Pretrazi", filtrirane);

                }
                else
                {
                    if (koncert != null)
                    {
                        foreach (var m in pretrazene)
                        {
                            if (m.Tip.ToString().Equals("KONCERT"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }
                    if (pozoriste != null)
                    {
                        foreach (var m in pretrazene)
                        {
                            if (m.Tip.ToString().Equals("POZORISTE"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }
                    if (bioskop != null)
                    {
                        foreach (var m in pretrazene)
                        {
                            if (m.Tip.ToString().Equals("BIOSKOP"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }
                    if (festival != null)
                    {
                        foreach (var m in pretrazene)
                        {
                            if (m.Tip.ToString().Equals("FESTIVAL"))
                            {
                                filtrirane.Add(m);
                            }
                        }
                    }
                    return View("Pretrazi", filtrirane);
                }
            }
        }
    }
}