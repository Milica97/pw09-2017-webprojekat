﻿using RezervacijaUlaznica.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RezervacijaUlaznica.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Register
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Add(Korisnik k)
        {
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];

            if (!ModelState.IsValid)
            {
                TempData["Greska"] = "Sva polja se moraju popuniti!";
                return View("Index");
            }

            string korisnickoIme = k.KorisnickoIme;
            string lozinka = k.Lozinka;
            string ime = k.Ime;
            string prezime = k.Prezime;
            PolTip pol = k.Pol;
            DateTime datumRodjenja = k.DatumRodjenja;
            UlogaKorisnika uloga = UlogaKorisnika.KUPAC;
            List<Karta> karte = new List<Karta>();
            double brBodova = 0;
            TipKorisnika tip = new TipKorisnika(ImeTipa.OBICNI, 0, 0);
            bool obrisan = false;
            int id = kupci.Count + 1;

            Korisnik novi = new Korisnik(korisnickoIme, lozinka, ime, prezime, pol, datumRodjenja, uloga, brBodova, tip,obrisan,id);

          
            if (kupci.Contains(k) || admini.Contains(k) || prodavci.Contains(k))
            {
                TempData["Greska"] = $"Korisnik sa korisnickim imenom: {k.KorisnickoIme} vec postoji!";
                return View("Index");
            }
            
            kupci.Add(novi);
            HttpContext.Application["kupci"] = kupci;
            Podaci.SacuvajKupce(novi);
            return RedirectToAction("Index", "Home");
        }

    }
}