﻿using RezervacijaUlaznica.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RezervacijaUlaznica.Controllers
{
    public class AdministratorController : Controller
    {
        // GET: Administrator
        public ActionResult Index()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (prodavac == null)
            {
                if(kupac==null)
                {
                    if(admin==null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    { 
                        return View(admin);
                    }
                }
                else
                {
                    return RedirectToAction("Profil", "Kupac");
                }
            }
            return RedirectToAction("Profil", "Prodavac");
        }

        public ActionResult NoveManifestacije()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["Manifestacije"];
            List<Manifestacija> neaktivneManifestacije = new List<Manifestacija>();

            foreach (Manifestacija m in manifestacije)
              {
                if (m.Status.ToString().Equals("NEAKTIVNO") && m.DatumIVrijemeOdrzavanja>DateTime.Now)
                {
                  neaktivneManifestacije.Add(m);
                }
            }
            if (neaktivneManifestacije.Count == 0)
            {
               TempData["praznaLista"] = $"Nema novih manifestacija";
            }
            return View(neaktivneManifestacije);
            
        }

        public ActionResult Odobreno(int id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            foreach(var m in manifestacije)
            {
                if (m.Id == id)
                {
                    m.Status = StatusManifestacije.AKTIVNO;
                    break;
                }
            }

            Podaci.AzurirajManifestacije(manifestacije);
            return RedirectToAction("NoveManifestacije");
        }

        [HttpPost]
        public ActionResult Detalji(int id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];

            foreach (Manifestacija m in manifestacije)
            {
                if (m.Id == id)
                    return View(m);

            }
            return RedirectToAction("NoveManifestacije");

        }

        [HttpPost]
        public ActionResult Azuriraj(int id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            Korisnik k = new Korisnik();
            foreach (var a in admini)
            {
                if (a.Id==id)
                {
                    return View(a);
                }
            }
            return View(k);
        }

        [HttpPost]
        public ActionResult AzurirajAdmina(Korisnik azuriranAdmin)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            try
            {
                DateTime datum = azuriranAdmin.DatumRodjenja;
            }
            catch
            {
                TempData["greska"] = $"Polje 'Datum rodjenja' nije u dobrom formatu!";
                return View("Azuriraj", admin);
            }
            if (!ModelState.IsValid)
            {
                TempData["greska"] = $"Sva polja moraju biti popunjena!";
                return View("Azuriraj",admin);

            }

            if (kupci.Contains(azuriranAdmin) || prodavci.Contains(azuriranAdmin))
            {
                TempData["greska"] = $"Korisnik sa korisnickim imenom: {azuriranAdmin.KorisnickoIme} vec postoji!";
                return View("Azuriraj",admin);
            }

           

            foreach (var k in admini)
            {
                if (k.Id != azuriranAdmin.Id)
                {
                    if (k.KorisnickoIme.Equals(azuriranAdmin.KorisnickoIme))
                    {
                        ViewBag.Message = $"Korisnik sa korisnickim imenom: {azuriranAdmin.KorisnickoIme} vec postoji!";
                        return View("Azuriraj", admin);
                    }
                }
            }

            foreach (Korisnik k in admini)
            {
                if (k.Id == azuriranAdmin.Id)
                {
                    k.KorisnickoIme = azuriranAdmin.KorisnickoIme;
                    k.Lozinka = azuriranAdmin.Lozinka;
                    k.Ime = azuriranAdmin.Ime;
                    k.Prezime = azuriranAdmin.Prezime;
                    k.Pol = azuriranAdmin.Pol;
                    k.DatumRodjenja = azuriranAdmin.DatumRodjenja;
                    break;
                }
            }

            HttpContext.Application["admin"] = admini;
            Podaci.AzururajAdministratora(admini);
        
            return RedirectToAction("Index");
        }

        public ActionResult DodajProdavca()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Dodaj(Korisnik prodavac)
        {
            //Kad doda novog da se prebaci na stranicu gdje vidi sve registrovane

            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodava = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodava == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            if (!ModelState.IsValid)
            {
                TempData["greska"] = $"Sva polja moraju biti popunjena!";
                return View("DodajProdavca");
            }

            if (kupci.Contains(prodavac) || admini.Contains(prodavac) || prodavci.Contains(prodavac))
            {
                TempData["greska"] = $"Korisnik sa korisnickim imenom: {prodavac.KorisnickoIme} vec postoji!";
                return View("DodajProdavca");
            }

            Korisnik noviProdavac = new Korisnik(prodavac.KorisnickoIme, prodavac.Lozinka, prodavac.Ime, prodavac.Prezime, prodavac.Pol, prodavac.DatumRodjenja, UlogaKorisnika.PRODAVAC, 0, new TipKorisnika(ImeTipa.OBICNI, 0, 0), false, prodavci.Count + 1);
            prodavci.Add(noviProdavac);
            HttpContext.Application["prodavci"] = prodavci;
            Podaci.AzururajProdavca(prodavci);
            return RedirectToAction("Korisnici");
        }

        public ActionResult Korisnici()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }

            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            List<Korisnik> korisniciSistema = new List<Korisnik>();
            
            foreach(Korisnik k in prodavci)
            {
                korisniciSistema.Add(k);
            }
            foreach (Korisnik k in admini)
            {
                korisniciSistema.Add(k);
            }
            foreach (Korisnik k in kupci)
            {
                korisniciSistema.Add(k);
            }
            HttpContext.Application["korisnici"] = korisniciSistema;
            return View(korisniciSistema);
        }

        public ActionResult Obrisi(string korisnickoIme)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            foreach(Korisnik k in prodavci)
            {
                if (k.KorisnickoIme.Equals(korisnickoIme))
                {
                    k.Obrisan = true;
                    HttpContext.Application["prodavci"] = prodavci;
                    Podaci.AzururajProdavca(prodavci);
                    break;
                    
                }
            }

            foreach (Korisnik k in kupci)
            {
                if (k.KorisnickoIme.Equals(korisnickoIme))
                {
                    k.Obrisan = true;
                    HttpContext.Application["kupci"] = kupci;
                    Podaci.AzururajKupca(kupci);
                    break;

                }
            }
            return RedirectToAction("Korisnici");

        }

        [HttpPost]
        public ActionResult Pretrazi(string ime, string prezime, string korisnickoIme)
        {
            Korisnik kupa = (Korisnik)Session["kupci"];
            Korisnik prodava = (Korisnik)Session["prodavci"];
            Korisnik admi = (Korisnik)Session["admin"];

            if (admi == null)
            {
                if (kupa == null)
                {
                    if (prodava == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            List<Korisnik> pretrazeni = new List<Korisnik>();
            List<Korisnik> pretrazeniPrezime = new List<Korisnik>();
            List<Korisnik> pretrazeniKorisnicko = new List<Korisnik>();

            if (!ime.Equals("") && !prezime.Equals("") && !korisnickoIme.Equals(""))
            {
                foreach (Korisnik prodavac in prodavci)
                {
                    if (prodavac.Ime.ToUpper().Equals(ime.ToUpper()) && prodavac.Prezime.ToUpper().Equals(prezime.ToUpper()) && prodavac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                    {
                        pretrazeni.Add(prodavac);
                    }
                }
                foreach (Korisnik admin in admini)
                {
                    if (admin.Ime.ToUpper().Equals(ime.ToUpper()) && admin.Prezime.ToUpper().Equals(prezime.ToUpper()) && admin.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                    {
                        pretrazeni.Add(admin);
                    }
                }
                foreach (Korisnik kupac in kupci)
                {
                    if (kupac.Ime.ToUpper().Equals(ime.ToUpper()) && kupac.Prezime.ToUpper().Equals(prezime.ToUpper()) && kupac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                    {
                        pretrazeni.Add(kupac);
                    }
                }
                return View("Korisnici", pretrazeni);

            }
            else
            {

                if (!ime.Equals(""))
                {
                    foreach (Korisnik prodavac in prodavci)
                    {
                        if (prodavac.Ime.ToUpper().Equals(ime.ToUpper()))
                        {
                            pretrazeni.Add(prodavac);
                        }
                    }
                    foreach (Korisnik admin in admini)
                    {
                        if (admin.Ime.ToUpper().Equals(ime.ToUpper()))
                        {
                            pretrazeni.Add(admin);
                        }
                    }
                    foreach (Korisnik kupac in kupci)
                    {
                        if (kupac.Ime.ToUpper().Equals(ime.ToUpper()))
                        {
                            pretrazeni.Add(kupac);
                        }
                    }

                    if (!prezime.Equals(""))
                    {
                        foreach (Korisnik prodavac in pretrazeni)
                        {
                            if (prodavac.Prezime.ToUpper().Equals(prezime.ToUpper()))
                            {
                                pretrazeniPrezime.Add(prodavac);

                            }
                        }
                        foreach (Korisnik admin in pretrazeni)
                        {
                            if (admin.Prezime.ToUpper().Equals(prezime.ToUpper()))
                            {

                                pretrazeniPrezime.Add(admin);

                            }
                        }
                        foreach (Korisnik kupac in pretrazeni)
                        {
                            if (kupac.Prezime.ToUpper().Equals(prezime.ToUpper()))
                            {

                                pretrazeniPrezime.Add(kupac);

                            }
                        }


                        if (!korisnickoIme.Equals(""))
                        {
                            foreach (Korisnik prodavac in pretrazeniPrezime)
                            {
                                if (prodavac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {
                                    pretrazeniKorisnicko.Add(prodavac);

                                }
                            }
                            foreach (Korisnik admin in pretrazeniPrezime)
                            {
                                if (admin.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {

                                    pretrazeniKorisnicko.Add(admin);

                                }
                            }
                            foreach (Korisnik kupac in pretrazeniPrezime)
                            {
                                if (kupac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {

                                    pretrazeniKorisnicko.Add(kupac);

                                }
                            }

                            return View("Korisnici", pretrazeniKorisnicko);
                        }
                        else
                        {
                            return View("Korisnici", pretrazeniPrezime);
                        }
                    }
                    else
                    {
                        if (!korisnickoIme.Equals(""))
                        {
                            foreach (Korisnik prodavac in pretrazeni)
                            {
                                if (prodavac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {
                                    pretrazeniKorisnicko.Add(prodavac);

                                }
                            }
                            foreach (Korisnik admin in pretrazeni)
                            {
                                if (admin.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {
                                    pretrazeniKorisnicko.Add(admin);

                                }
                            }
                            foreach (Korisnik kupac in pretrazeni)
                            {
                                if (kupac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {

                                    pretrazeniKorisnicko.Add(kupac);

                                }
                            }
                            return View("Korisnici", pretrazeniKorisnicko);

                        }
                        else
                        {
                            return View("Korisnici", pretrazeni);
                        }
                    }

                }
                else
                {
                    if (!prezime.Equals(""))
                    {
                        foreach (Korisnik prodavac in prodavci)
                        {
                            if (prodavac.Prezime.ToUpper().Equals(prezime.ToUpper()))
                            {
                                pretrazeniPrezime.Add(prodavac);

                            }
                        }
                        foreach (Korisnik admin in admini)
                        {
                            if (admin.Prezime.ToUpper().Equals(prezime.ToUpper()))
                            {
                                pretrazeniPrezime.Add(admin);

                            }
                        }
                        foreach (Korisnik kupac in kupci)
                        {
                            if (kupac.Prezime.ToUpper().Equals(prezime.ToUpper()))
                            {
                                pretrazeniPrezime.Add(kupac);

                            }
                        }

                        if (!korisnickoIme.Equals(""))
                        {
                            foreach (Korisnik prodavac in pretrazeniPrezime)
                            {
                                if (prodavac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {
                                    pretrazeniKorisnicko.Add(prodavac);

                                }
                            }
                            foreach (Korisnik admin in pretrazeniPrezime)
                            {
                                if (admin.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {

                                    pretrazeniKorisnicko.Add(admin);

                                }
                            }
                            foreach (Korisnik kupac in pretrazeniPrezime)
                            {
                                if (kupac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {

                                    pretrazeniKorisnicko.Add(kupac);

                                }
                            }
                            return View("Korisnici", pretrazeniKorisnicko);
                        }
                        else
                        {
                            return View("Korisnici", pretrazeniPrezime);
                        }
                    }
                    else
                    {
                        if (!korisnickoIme.Equals(""))
                        {
                            foreach (Korisnik prodavac in prodavci)
                            {
                                if (prodavac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {

                                    pretrazeniKorisnicko.Add(prodavac);

                                }
                            }
                            foreach (Korisnik admin in admini)
                            {
                                if (admin.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {
                                    pretrazeniKorisnicko.Add(admin);

                                }
                            }
                            foreach (Korisnik kupac in kupci)
                            {
                                if (kupac.KorisnickoIme.ToUpper().Equals(korisnickoIme.ToUpper()))
                                {

                                    pretrazeniKorisnicko.Add(kupac);

                                }
                            }
                            return View("Korisnici", pretrazeniKorisnicko);
                        }
                        else
                        {
                            foreach (Korisnik prodavac in prodavci)
                            {
                                pretrazeni.Add(prodavac);
                            }
                            foreach (Korisnik admin in admini)
                            {
                                pretrazeni.Add(admin);
                            }
                            foreach (Korisnik kupac in kupci)
                            {
                                pretrazeni.Add(kupac);
                            }

                            return View("Korisnici", pretrazeni);
                        }
                    }
                }
            }
           
        }

        [HttpPost]
        public ActionResult Sortiraj(string sortirajPo, string opRas)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            List<Korisnik> sortirani = new List<Korisnik>();
            List<Korisnik> korisniciSistema = new List<Korisnik>();

            foreach (Korisnik k in prodavci)
            {
                korisniciSistema.Add(k);
            }
            foreach (Korisnik k in admini)
            {
                korisniciSistema.Add(k);
            }
            foreach (Korisnik k in kupci)
            {
                korisniciSistema.Add(k);
            }

            if (opRas.Equals(""))
            {
                opRas = "Rastuce";
            }

            switch (sortirajPo)
            {
                case "Ime":
                    if (opRas.Equals("Rastuce"))
                    {
                        sortirani = korisniciSistema.OrderBy(v => v.Ime).ToList();
                        break;
                    }
                    else
                    {
                        sortirani = korisniciSistema.OrderByDescending(v => v.Ime).ToList();
                        break;
                    }

                case "Prezime":
                    if (opRas.Equals("Rastuce"))
                    {
                        sortirani = korisniciSistema.OrderBy(v => v.Prezime).ToList();
                        break;
                    }
                    else
                    {
                        sortirani = korisniciSistema.OrderByDescending(v => v.Prezime).ToList();
                        break;
                    }
                case "Korisnicko_ime":
                    if (opRas.Equals("Rastuce"))
                    {
                        sortirani = korisniciSistema.OrderBy(v => v.KorisnickoIme).ToList();
                        break;
                    }
                    else
                    {
                        sortirani = korisniciSistema.OrderByDescending(v => v.KorisnickoIme).ToList();
                        break;
                    }
                case "Broj_sakupljenih_bodova":
                    if (opRas.Equals("Rastuce"))
                    {
                        sortirani = korisniciSistema.OrderBy(v => v.BrojSakupljenihBodova).ToList();
                        break;
                    }
                    else
                    {
                        sortirani = korisniciSistema.OrderByDescending(v => v.BrojSakupljenihBodova).ToList();
                        break;
                    }
                default:
                    sortirani = korisniciSistema;
                    break;
            }
            return View("Korisnici", sortirani);
        }

        public ActionResult Filtriraj(string admin, string prodavac, string kupac, string zlatni, string srebrni, string bronzani)
        {
            Korisnik kupa = (Korisnik)Session["kupci"];
            Korisnik prodava = (Korisnik)Session["prodavci"];
            Korisnik admi = (Korisnik)Session["admin"];

            if (admi == null)
            {
                if (kupa == null)
                {
                    if (prodava == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Korisnik> korisniciSistema = (List<Korisnik>)HttpContext.Application["korisnici"];

            List<Korisnik> pomocna = new List<Korisnik>();
            
            if (admin == null && prodavac == null && kupac == null && zlatni == null && srebrni == null && bronzani==null)
            {
                pomocna = korisniciSistema;
                return View("Korisnici", pomocna);
            }


            if (admin != null && prodavac != null && kupac != null && zlatni != null && srebrni != null && bronzani != null)
            {
                pomocna = korisniciSistema;
                return View("Korisnici", pomocna);
            }
            else
            {
                if (admin != null)
                {
                    foreach (var a in admini)
                    {
                        pomocna.Add(a);
                    }
                }
                if (prodavac != null)
                {
                    foreach (var p in prodavci)
                    {
                        pomocna.Add(p);
                    }
                }

                if (kupac != null)
                {
                    if (zlatni != null && srebrni != null && bronzani != null)
                    {
                        foreach (var k in kupci)
                        {
                            if (!k.Tip.Ime.ToString().Equals("OBICNI"))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                    else if(zlatni != null && srebrni != null)
                    {
                        foreach (var k in kupci)
                        {
                            if (k.Tip.Ime.ToString().Equals("ZLATNI") || k.Tip.Ime.ToString().Equals("SREBRNI"))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                    else if (zlatni != null && bronzani != null)
                    {
                        foreach (var k in kupci)
                        {
                            if (k.Tip.Ime.ToString().Equals("ZLATNI") || k.Tip.Ime.ToString().Equals("BRONZANI"))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                    else if (srebrni != null && bronzani != null)
                    {
                        foreach (var k in kupci)
                        {
                            if (k.Tip.Ime.ToString().Equals("SREBRNI") || k.Tip.Ime.ToString().Equals("BRONZANI"))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                    else if (zlatni!=null)
                    {
                        foreach (var k in kupci)
                        {
                            if (k.Tip.Ime.ToString().Equals("ZLATNI"))
                            {
                                pomocna.Add(k);
                            }
                        }

                    }
                    else if (srebrni != null)
                    {
                        foreach (var k in kupci)
                        {
                            if (k.Tip.Ime.ToString().Equals("SREBRNI"))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                    else if (bronzani != null)
                    {
                        foreach (var k in kupci)
                        {
                            if (k.Tip.Ime.ToString().Equals("BRONZANI"))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                    else
                    {
                        foreach (var k in kupci)
                        {
                            pomocna.Add(k);
                        }
                    }
                }

                if (zlatni != null && srebrni != null && bronzani != null)
                {
                    foreach (var k in kupci)
                    {
                        if (!k.Tip.Ime.ToString().Equals("OBICNI"))
                        {
                            if (!pomocna.Contains(k))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                }
                else if (zlatni != null && srebrni != null)
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("ZLATNI") || k.Tip.Ime.ToString().Equals("SREBRNI"))
                        {
                            if (!pomocna.Contains(k))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                }
                else if (zlatni != null && bronzani != null)
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("ZLATNI") || k.Tip.Ime.ToString().Equals("BRONZANI"))
                        {
                            pomocna.Add(k); if (!pomocna.Contains(k))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                }
                else if (srebrni != null && bronzani != null)
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("SREBRNI") || k.Tip.Ime.ToString().Equals("BRONZANI"))
                        {
                            if (!pomocna.Contains(k))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                }
                else if (zlatni != null)
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("ZLATNI"))
                        {
                            if (!pomocna.Contains(k))
                            {
                                if (!pomocna.Contains(k))
                                {
                                    pomocna.Add(k);
                                }
                            }
                        }
                    }

                }
                else if (srebrni != null)
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("SREBRNI"))
                        {
                            if (!pomocna.Contains(k))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                }
                else if (bronzani != null)
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("BRONZANI"))
                        {
                            if (!pomocna.Contains(k))
                            {
                                pomocna.Add(k);
                            }
                        }
                    }
                }
                //else
                //{
                //    foreach (var k in kupci)
                //    {
                //        if (!pomocna.Contains(k))
                //        {
                //            pomocna.Add(k);
                //        }
                //    }
                //}

                return View("Korisnici", pomocna);
            }


            #region DobroFiltriranje
            /*
             * 
            List<Korisnik> pomocna1 = new List<Korisnik>();
            List<Korisnik> pomocna2 = new List<Korisnik>();
            List<Korisnik> pomocna3 = new List<Korisnik>();
            List<Korisnik> pomocna4 = new List<Korisnik>();
            List<Korisnik> pomocna5 = new List<Korisnik>();
            List<Korisnik> pomocna6 = new List<Korisnik>();
            List<Korisnik> pomocna7 = new List<Korisnik>();

            if (zlatni != null && srebrni != null && bronzani != null)
            {
                if(admin != null || prodavac != null)
                {
                    TempData["greska"] = $"Samo KUPCI mogu imati TIP";
                    return View("Korisnici", korisniciSistema);
                }
                else
                {
                    foreach(var k in kupci)
                    {
                        if (!k.Tip.Ime.ToString().Equals("OBICNI"))
                        {
                            pomocna1.Add(k);
                        }
                    }
                        
                }
                return View("Korisnici", pomocna1);

            }
            else if(zlatni != null && srebrni != null )
            {
                if (admin != null || prodavac != null)
                {
                    TempData["greska"] = $"Samo KUPCI mogu imati TIP";
                    return View("Korisnici", korisniciSistema);
                }
                else
                {
                    foreach (var k in kupci)
                    {
                        if (!k.Tip.Ime.ToString().Equals("OBICNI") && !k.Tip.Ime.ToString().Equals("BRONZANI"))
                        {
                            pomocna2.Add(k);
                        }
                    }
                }
                return View("Korisnici", pomocna2);
            }
            else if(zlatni != null && bronzani != null)
            {
                if (admin != null || prodavac != null)
                {
                    TempData["greska"] = $"Samo KUPCI mogu imati TIP";
                    return View("Korisnici", korisniciSistema);
                }
                else
                {
                    foreach (var k in kupci)
                    {
                        if (!k.Tip.Ime.ToString().Equals("OBICNI") && !k.Tip.Ime.ToString().Equals("SREBRNI"))
                        {
                            pomocna3.Add(k);
                        }
                    }
                }
                return View("Korisnici", pomocna3);

            }
            else if(srebrni != null && bronzani != null)
            {
                if (admin != null || prodavac != null)
                {
                    TempData["greska"] = $"Samo KUPCI mogu imati TIP";
                    return View("Korisnici", korisniciSistema);
                }
                else
                {
                    foreach (var k in kupci)
                    {
                        if (!k.Tip.Ime.ToString().Equals("OBICNI") && !k.Tip.Ime.ToString().Equals("ZLATNI"))
                        {
                            pomocna4.Add(k);
                        }
                    }
                }
                return View("Korisnici", pomocna4);

            }
            else if(srebrni != null)
            {
                if (admin != null || prodavac != null)
                {
                    TempData["greska"] = $"Samo KUPCI mogu imati TIP";
                    return View("Korisnici", korisniciSistema);
                }
                else
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("SREBRNI"))
                        {
                            pomocna5.Add(k);
                        }
                    }
                }
                return View("Korisnici", pomocna5);

            }
            else if(zlatni != null)
            {
                if (admin != null || prodavac != null)
                {
                    TempData["greska"] = $"Samo KUPCI mogu imati TIP";
                    return View("Korisnici", korisniciSistema);
                }
                else
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("ZLATNI"))
                        {
                            pomocna6.Add(k);
                        }
                    }
                }
                return View("Korisnici", pomocna6);

            }
            else if(bronzani != null)
            {
                if (admin != null || prodavac != null)
                {
                    TempData["greska"] = $"Samo KUPCI mogu imati TIP";
                    return View("Korisnici", korisniciSistema);
                }
                else
                {
                    foreach (var k in kupci)
                    {
                        if (k.Tip.Ime.ToString().Equals("BRONZANI"))
                        {
                            pomocna7.Add(k);
                        }
                    }
                }
                return View("Korisnici", pomocna7);
            }
            else
            {
                if (admin != null)
                {
                    foreach(var a in admini)
                    {
                        pomocna.Add(a);
                    }
                }
                if (prodavac != null)
                {
                    foreach (var p in prodavci)
                    {
                        pomocna.Add(p);
                    }
                }
                if (kupac != null)
                {
                    foreach (var k in kupci)
                    {
                        pomocna.Add(k);
                    }
                }
                return View("Korisnici", pomocna);

            }
            */
            #endregion


        }
        public ActionResult Karte()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];


            foreach (var m in manifestacije)
            {
                if (m.DatumIVrijemeOdrzavanja < DateTime.Now && !m.Status.ToString().Equals("OTKAZANO") && !m.Status.ToString().Equals("NEAKTIVNO") && !m.Status.ToString().Equals("ZAVRSENO"))
                {
                    m.Zavrsena = true;
                    m.Status = StatusManifestacije.ZAVRSENO;

                }
            }
            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja < DateTime.Now && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    r.karta.ZaKojuManifestaciju.Zavrsena = true;
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);
            return View(rezervacije);
        }

        public ActionResult Komentari()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna = new List<Komentar>();
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);
                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }

            return View(pomocna1);
        }

        public ActionResult Odjava()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }
            Session["admin"] = null;
            return RedirectToAction("Index","Home");
        }

        public ActionResult Otkazi(int id)
        {
            //id manifestacije
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];
            Korisnik admin = (Korisnik)Session["admin"];

            if (admin == null)
            {
                if (kupac == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Kupac");
                }
            }

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    m.Status = StatusManifestacije.OTKAZANO;
                    break;
                }
            }
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.Id == id)
                {
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.OTKAZANO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);

            return RedirectToAction("Index","Home");
        }

    }
}