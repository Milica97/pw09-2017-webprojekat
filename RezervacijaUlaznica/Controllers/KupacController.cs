﻿using RezervacijaUlaznica.Models;
using RezervacijaUlaznica.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RezervacijaUlaznica.Controllers
{
    public class KupacController : Controller
    {
        // GET: Kupac
        public ActionResult Index()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            return View(kupac);
        }

        public ActionResult Profil()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Profil", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Index", "Administrator");
                }
            }
            return View(kupac);
        }

        public ActionResult Manifestacije()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Manifestacije", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];

            foreach (var m in manifestacije)
            {
                if (m.DatumIVrijemeOdrzavanja < DateTime.Now && !m.Status.ToString().Equals("OTKAZANO") && !m.Status.ToString().Equals("NEAKTIVNO") && !m.Status.ToString().Equals("ZAVRSENO"))
                {
                    m.Zavrsena = true;
                    m.Status = StatusManifestacije.ZAVRSENO;

                }
            }
            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja < DateTime.Now && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    r.karta.ZaKojuManifestaciju.Zavrsena = true;
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);
            //List<Manifestacija> aktivne = new List<Manifestacija>();

            //foreach (var m in manifestacije)
            //{
            //    if (m.Status.ToString().Equals("AKTIVNO") && m.Zavrsena==false)
            //    {
            //        aktivne.Add(m);
            //    }
            //}

            return View(manifestacije);
        }

        public ActionResult AzurirajSvojePodatke(int id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            Korisnik k = new Korisnik();
            foreach (var p in kupci)
            {
                if (p.Id == id)
                {
                    return View(p);
                }
            }
            return View(k);
        }

        [HttpPost]
        public ActionResult AzurirajKupca(Korisnik azuriranKupac)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Korisnik> admini = (List<Korisnik>)HttpContext.Application["admin"];
            List<Korisnik> prodavci = (List<Korisnik>)HttpContext.Application["prodavci"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];

            if (!ModelState.IsValid)
            {
                TempData["greska"] = $"Sva polja moraju biti popunjena!";
                return View("AzurirajSvojePodatke", kupac);

            }
            if (admini.Contains(azuriranKupac) || prodavci.Contains(azuriranKupac))
            {
                TempData["greska"] = $"Korisnik sa korisnickim imenom: {azuriranKupac.KorisnickoIme} vec postoji!";
                return View("AzurirajSvojePodatke", kupac);
            }

            foreach(var k in kupci)
            {
                if (k.Id != azuriranKupac.Id)
                {
                    if (k.KorisnickoIme.Equals(azuriranKupac.KorisnickoIme))
                    {
                        TempData["greska"] = $"Korisnik sa korisnickim imenom: {azuriranKupac.KorisnickoIme} vec postoji!";
                        return View("AzurirajSvojePodatke", kupac);
                    }
                }
            }

            foreach (Korisnik k in kupci)
            {
                if (k.Id == azuriranKupac.Id)
                {
                    k.KorisnickoIme = azuriranKupac.KorisnickoIme;
                    k.Lozinka = azuriranKupac.Lozinka;
                    k.Ime = azuriranKupac.Ime;
                    k.Prezime = azuriranKupac.Prezime;
                    k.Pol = azuriranKupac.Pol;
                    k.DatumRodjenja = azuriranKupac.DatumRodjenja;
                    break;
                }
            }

            HttpContext.Application["kupci"] = kupci;
            Podaci.AzururajKupca(kupci);

            foreach(var r in rezervacije)
            {
                if (r.karta.kupac.Id == azuriranKupac.Id)
                {
                    r.karta.kupac.KorisnickoIme = azuriranKupac.KorisnickoIme;
                    r.karta.kupac.Lozinka = azuriranKupac.Lozinka;
                    r.karta.kupac.Ime = azuriranKupac.Ime;
                    r.karta.kupac.Prezime = azuriranKupac.Prezime;
                    r.karta.kupac.Pol = azuriranKupac.Pol;
                    r.karta.kupac.DatumRodjenja = azuriranKupac.DatumRodjenja;
                }
            }
            HttpContext.Application["rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);

            return RedirectToAction("Profil");
        }

        public ActionResult SveKarte()
        {
            //imamo polje u Korisnink.SveKarte

            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Karte", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Karte", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            List<Rezervacija> pomocna = new List<Rezervacija>();

            foreach (var r in rezervacije)
            {
                if (r.karta.kupac.Id == kupac.Id)
                {
                    pomocna.Add(r);
                }
            }
            HttpContext.Application["rezervacijekupca"] = pomocna;
            return View(pomocna);
        }

        public ActionResult Detalji(int id)
        {
            //id manifestacije
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];

            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);

                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }
            Manifestacija pomocna = new Manifestacija();
            ManifestacijeKomentari mankom = new ManifestacijeKomentari();
            List<Komentar> pomkomentari = new List<Komentar>();

            foreach (var m in manifestacije)
            {
                if (m.DatumIVrijemeOdrzavanja < DateTime.Now && !m.Status.ToString().Equals("OTKAZANO") && !m.Status.ToString().Equals("NEAKTIVNO") && !m.Status.ToString().Equals("ZAVRSENO"))
                {
                    m.Zavrsena = true;
                    m.Status = StatusManifestacije.ZAVRSENO;

                }
            }
            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja < DateTime.Now && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    r.karta.ZaKojuManifestaciju.Zavrsena = true;
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    if (m.Zavrsena == false)
                    {
                        mankom = new ManifestacijeKomentari(m, null);
                        return View(mankom);
                    }
                    else
                    {
                        foreach (var kom in pomocna1)
                        {
                            if (kom.Manifestacija.Id == id && kom.Tip.ToString().Equals("ODOBREN"))
                            {
                                pomkomentari.Add(kom);
                            }
                        }

                        mankom = new ManifestacijeKomentari(m, pomkomentari);
                        return View(mankom);

                    }
                }
            }

            //foreach (var m in manifestacije)
            //{
            //    if (m.Id == id)
            //    {
            //        return View(m);
            //    }
            //}
            return RedirectToAction("Manifestacije");
        }
        
        public ActionResult DetaljiPretraga(int id)
        {
            //id manifestacije
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];

            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Komentar> pomocna1 = new List<Komentar>();
            Komentar kom1 = new Komentar();

            foreach (var kom in komentari)
            {
                foreach (var man in manifestacije)
                {
                    if (kom.IdManifestacije == man.Id)
                    {
                        foreach (var kup in kupci)
                        {
                            if (kup.Id == kom.IdKupca)
                            {
                                kom1 = new Komentar(kom.Id, kup, man, kom.TekstKomentara, kom.Ocjena, kom.Tip);

                                pomocna1.Add(kom1);
                            }
                        }

                    }

                }
            }
            Manifestacija pomocna = new Manifestacija();
            ManifestacijeKomentari mankom = new ManifestacijeKomentari();
            List<Komentar> pomkomentari = new List<Komentar>();

            foreach (var m in manifestacije)
            {
                if (m.DatumIVrijemeOdrzavanja < DateTime.Now && !m.Status.ToString().Equals("OTKAZANO") && !m.Status.ToString().Equals("NEAKTIVNO") && !m.Status.ToString().Equals("ZAVRSENO"))
                {
                    m.Zavrsena = true;
                    m.Status = StatusManifestacije.ZAVRSENO;

                }
            }
            foreach (var r in rezervacije)
            {
                if (r.karta.ZaKojuManifestaciju.DatumIVrijemeOdrzavanja < DateTime.Now && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !r.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    r.karta.ZaKojuManifestaciju.Zavrsena = true;
                    r.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            HttpContext.Application["Rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    if (m.Zavrsena == false)
                    {
                        mankom = new ManifestacijeKomentari(m, null);
                        return View(mankom);
                    }
                    else
                    {
                        foreach (var kom in pomocna1)
                        {
                            if (kom.Manifestacija.Id == id && kom.Tip.ToString().Equals("ODOBREN"))
                            {
                                pomkomentari.Add(kom);
                            }
                        }

                        mankom = new ManifestacijeKomentari(m, pomkomentari);
                        return View(mankom);

                    }
                }
            }

            //foreach (var m in manifestacije)
            //{
            //    if (m.Id == id)
            //    {
            //        return View(m);
            //    }
            //}
            return RedirectToAction("Manifestacije");

        }
        public ActionResult Rezervisi(int id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    return View(m);
                }
            }
            return RedirectToAction("Manifestacije");
        }
        
        public ActionResult RezervisiKartu(string kolicina,int id,string tipKarte)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            Manifestacija pomocna = new Manifestacija();

            foreach (var m in manifestacije)
            {
                if (m.Id == id)
                {
                    pomocna = m;
                    break;
                }
            }
            double cijena = 0;
            int kolicina1 = 0;

            if (!kolicina.Equals(""))
            {
                kolicina1=int.Parse(kolicina);
            }

            if (kolicina1 == 0)
            {
                TempData["kolicina"] = $"Polje kolicina mora biti popunjeno!";
                return View("Rezervisi", pomocna);
                
            }else if (kolicina1 < 0)
            {
                TempData["kolicina"] = $"Polje kolicina ne moze biti negativan broj!";
                return View("Rezervisi", pomocna);
            }

            switch (tipKarte)
            {
                case "REGULAR":
                    if (kolicina1 > pomocna.BrSlobodnihRegular)
                    {
                        TempData["kolicina"] = $"Kolicina koju trazite nije moguca. Broj slobodnih REGULAR mijesta je manji!";
                        return View("Rezervisi", pomocna);
                    }
                    break;
                case "VIP":

                    if (kolicina1 > pomocna.BrSlobodnihVip)
                    {
                        TempData["kolicina"] = $"Kolicina koju trazite nije moguca. Broj slobodnih VIP mijesta je manji!";
                        return View("Rezervisi", pomocna);
                    }
                    break;
                case "FAN_PIT":
                    if (kolicina1 > pomocna.BrSlobodnihFanPit)
                    {
                        TempData["kolicina"] = $"Kolicina koju trazite nije moguca. Broj slobodnih FAN PIT mijesta je manji!";
                        return View("Rezervisi", pomocna);
                    }
                    break;
                    
            }

            Karta karta = new Karta();
            Rezervacija rezervacija = new Rezervacija();

            switch (tipKarte)
            {
                case "REGULAR":
                    foreach (var m in manifestacije)
                    {
                        if (m.Id == id)
                        {
                            if (kupac.Tip.Ime.ToString().Equals("BRONZANI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;

                            } else if(kupac.Tip.Ime.ToString().Equals("SREBRNI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else if (kupac.Tip.Ime.ToString().Equals("ZLATNI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else
                            {
                                cijena = m.CijenaRegularKarte * kolicina1;
                            }

                            Random _random = new Random();
                            int num = _random.Next(10000,100000);

                            karta = new Karta("12345"+num, m, m.DatumIVrijemeOdrzavanja, cijena, kupac, StatusKarte.CEKANJE, TipKarte.REGULAR);
                            rezervacija = new Rezervacija(karta, kolicina1,rezervacije.Count+1);
                            rezervacije.Add(rezervacija);
                            HttpContext.Application["rezervacije"] = rezervacije;
                            break;
                        }
                    }
                    cijena = 0;
                    break;
                case "FAN_PIT":
                    foreach (var m in manifestacije)
                    {
                        if (m.Id == id)
                        {
                            if (kupac.Tip.Ime.ToString().Equals("BRONZANI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1 * 2;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else if (kupac.Tip.Ime.ToString().Equals("SREBRNI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1 * 2;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else if (kupac.Tip.Ime.ToString().Equals("ZLATNI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1 * 2;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena =regularCijena-popust;
                            }
                            else
                            {
                                cijena = m.CijenaRegularKarte * kolicina1 * 2;
                            }
                            Random _random = new Random();
                            int num = _random.Next(10000, 100000);

                            karta = new Karta("12345" + num, m, m.DatumIVrijemeOdrzavanja, cijena, kupac, StatusKarte.CEKANJE, TipKarte.FAN_PIT);
                            rezervacija = new Rezervacija(karta, kolicina1, rezervacije.Count + 1);
                            rezervacije.Add(rezervacija);
                            HttpContext.Application["rezervacije"] = rezervacije;
                            break;
                        }
                    }
                    cijena = 0;
                    break;
                case "VIP":
                    foreach (var m in manifestacije)
                    {
                        if (m.Id == id)
                        {
                            if (kupac.Tip.Ime.ToString().Equals("BRONZANI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1 * 4;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else if (kupac.Tip.Ime.ToString().Equals("SREBRNI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1 * 4;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else if (kupac.Tip.Ime.ToString().Equals("ZLATNI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1 * 4;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1 * 4;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }

                            Random _random = new Random();
                            int num = _random.Next(10000, 100000);

                            karta = new Karta("12345"+num, m, m.DatumIVrijemeOdrzavanja, cijena, kupac, StatusKarte.CEKANJE, TipKarte.VIP);
                            rezervacija = new Rezervacija(karta, kolicina1, rezervacije.Count + 1);
                            rezervacije.Add(rezervacija);
                            HttpContext.Application["rezervacije"] = rezervacije;
                            break;
                        }
                    }
                    cijena = 0;
                    break;
                default:
                    foreach (var m in manifestacije)
                    {
                        if (m.Id == id)
                        {
                            if (kupac.Tip.Ime.ToString().Equals("BRONZANI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else if (kupac.Tip.Ime.ToString().Equals("SREBRNI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else if (kupac.Tip.Ime.ToString().Equals("ZLATNI"))
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            else
                            {
                                double regularCijena = m.CijenaRegularKarte * kolicina1;
                                double popust = (regularCijena * kupac.Tip.Popust) / 1000;
                                cijena = regularCijena - popust;
                            }
                            Random _random = new Random();
                            int num = _random.Next(10000, 100000);

                            karta = new Karta("12345"+num, m, m.DatumIVrijemeOdrzavanja, cijena, kupac, StatusKarte.CEKANJE, TipKarte.REGULAR);
                            rezervacija = new Rezervacija(karta, kolicina1, rezervacije.Count + 1);
                            rezervacije.Add(rezervacija);
                            HttpContext.Application["rezervacije"] = rezervacije;
                            break;
                        }
                    }
                    cijena = 0;
                    break;

            }

            return View(rezervacija);
                
        }

        public ActionResult RezervisaneKarte()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Karte", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("Karte", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Rezervacija> pomocna = new List<Rezervacija>();

            //provjerimo da li je neka manifestacija zavrsena 
            foreach(var rez in rezervacije)
            {
                if (rez.karta.DatumIvrijemeManifestacije < DateTime.Now && !rez.karta.ZaKojuManifestaciju.Status.ToString().Equals("OTKAZANO") && !rez.karta.ZaKojuManifestaciju.Status.ToString().Equals("NEAKTIVNO") && !rez.karta.ZaKojuManifestaciju.Status.ToString().Equals("ZAVRSENO"))
                {
                    rez.karta.ZaKojuManifestaciju.Zavrsena = true;
                    rez.karta.ZaKojuManifestaciju.Status = StatusManifestacije.ZAVRSENO;
                }
            }
            foreach (var man in manifestacije)
            {
                if (man.DatumIVrijemeOdrzavanja < DateTime.Now && !man.Status.ToString().Equals("OTKAZANO") && !man.Status.ToString().Equals("NEAKTIVNO") && !man.Status.ToString().Equals("ZAVRSENO"))
                {
                    man.Zavrsena = true;
                    man.Status = StatusManifestacije.ZAVRSENO;
                }
            }

            HttpContext.Application["rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);

            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);


            foreach (var r in rezervacije)
            {
                if (r.karta.kupac.Id == kupac.Id && r.karta.Status.ToString().Equals("REZERVISANA"))
                {
                    pomocna.Add(r);
                }
            }
                
            return View(pomocna);
        }

        public ActionResult RezervisiKonacno(string id,int kolicina)
        {
            Korisnik kupa = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupa == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            Karta pomocna = new Karta();


            foreach(var rez in rezervacije)
            {
                if (rez.karta.IdKarte.Equals(id) && rez.karta.Status.ToString().Equals("CEKANJE"))
                {
                    rez.karta.Status = StatusKarte.REZERVISANA;
                    
                    Podaci.SacuvajRezervaciju(rez);
                    pomocna = rez.karta;
                    break;
                }
            }
            HttpContext.Application["rezervacije"] = rezervacije;

            foreach (Manifestacija m in manifestacije)
            {
                if (m.Equals(pomocna.ZaKojuManifestaciju))
                {
                    switch (pomocna.Tip.ToString())
                    {
                        case "REGULAR":
                            m.BrSlobodnihRegular -= kolicina;
                            if (m.BrSlobodnihRegular == 0 && m.BrSlobodnihFanPit==0 && m.BrSlobodnihVip==0)
                            {
                                m.Status = StatusManifestacije.RASPRODATO;
                            }
                            break;
                        case "VIP":
                            m.BrSlobodnihVip -= kolicina;
                            if (m.BrSlobodnihRegular == 0 && m.BrSlobodnihFanPit == 0 && m.BrSlobodnihVip == 0)
                            {
                                m.Status = StatusManifestacije.RASPRODATO;
                            }
                            break;
                        case "FAN_PIT":
                            m.BrSlobodnihFanPit -= kolicina;
                            if (m.BrSlobodnihRegular == 0 && m.BrSlobodnihFanPit == 0 && m.BrSlobodnihVip == 0)
                            {
                                m.Status = StatusManifestacije.RASPRODATO;
                            }
                            break;
                    }
                   //break;
                }
            }
            HttpContext.Application["manifestacije"] = manifestacije;
            Podaci.AzurirajManifestacije(manifestacije);

            
            foreach (var kupac in kupci)
            {
                if (pomocna.kupac.Id == kupac.Id)
                {
                    kupac.BrojSakupljenihBodova += (pomocna.Cijena / 100 * 133)*kolicina;
                    if (kupac.BrojSakupljenihBodova > 10000 && kupac.BrojSakupljenihBodova <=20000)
                    {
                        TipKorisnika tip = new TipKorisnika(ImeTipa.BRONZANI, 3, 10001);
                        kupac.Tip = tip;
                        break;

                    }
                    else if(kupac.BrojSakupljenihBodova > 20000 && kupac.BrojSakupljenihBodova <= 30000)
                    {
                        TipKorisnika tip = new TipKorisnika(ImeTipa.SREBRNI, 5, 20001);
                        kupac.Tip=tip;
                        break;
                    }
                    else if (kupac.BrojSakupljenihBodova > 30000 )
                    {
                        TipKorisnika tip = new TipKorisnika(ImeTipa.ZLATNI, 7, 30001);
                        kupac.Tip=tip;
                        break;
                    }
                    
                }
            }
            HttpContext.Application["kupci"] = kupci;
            Podaci.AzururajKupca(kupci);
            return RedirectToAction("RezervisaneKarte");
        }

        public ActionResult Odustani(string id)
        {
            //id rezervacije
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }

            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            foreach(var r in rezervacije)
            {
                if (r.id.ToString()==id)
                {
                    var razlika = r.karta.DatumIvrijemeManifestacije - DateTime.Now;
                    long dana = razlika.Days;
                    if (dana < 7)
                    {
                        TempData["odustanak"] = $"Nije moguce odustati od ove manifestacije jer pocinje za manje od 7 dana!";
                        return RedirectToAction("RezervisaneKarte");
                    }
                    r.karta.Status = StatusKarte.ODUSTANAK;
                    //da se za manifestaciju povecaju slobodna mjesta
                    switch (r.karta.Tip.ToString())
                    {
                        case "REGULAR":
                            r.karta.ZaKojuManifestaciju.BrSlobodnihRegular += r.kolicina;
                            foreach(var m in manifestacije)
                            {
                                if (m.Id == r.karta.ZaKojuManifestaciju.Id)
                                {
                                    m.BrSlobodnihRegular += r.kolicina;
                                    break;
                                }
                            }
                            HttpContext.Application["manifestacije"] = manifestacije;
                            Podaci.AzurirajManifestacije(manifestacije);
                            break;
                        case "VIP":
                            r.karta.ZaKojuManifestaciju.BrSlobodnihVip += r.kolicina;
                            foreach (var m in manifestacije)
                            {
                                if (m.Id == r.karta.ZaKojuManifestaciju.Id)
                                {
                                    m.BrSlobodnihVip += r.kolicina;
                                    break;
                                }
                            }
                            HttpContext.Application["manifestacije"] = manifestacije;
                            Podaci.AzurirajManifestacije(manifestacije);
                            break;
                        case "FAN_PIT":
                            r.karta.ZaKojuManifestaciju.BrSlobodnihFanPit+= r.kolicina;
                            foreach (var m in manifestacije)
                            {
                                if (m.Id == r.karta.ZaKojuManifestaciju.Id)
                                {
                                    m.BrSlobodnihFanPit += r.kolicina;
                                    break;
                                }
                            }
                            HttpContext.Application["manifestacije"] = manifestacije;
                            Podaci.AzurirajManifestacije(manifestacije);
                            break;
                    }

                    double izgubljeniBodovi = (r.karta.Cijena / 1000 * 133 * 4) * r.kolicina;
                    r.karta.kupac.BrojSakupljenihBodova -= izgubljeniBodovi;
                    if (kupac.BrojSakupljenihBodova > 10000 && kupac.BrojSakupljenihBodova <= 20000)
                    {
                        TipKorisnika tip = new TipKorisnika(ImeTipa.BRONZANI, 3, 10001);
                        kupac.Tip = tip;
                        break;

                    }
                    else if (kupac.BrojSakupljenihBodova > 20000 && kupac.BrojSakupljenihBodova <= 30000)
                    {
                        TipKorisnika tip = new TipKorisnika(ImeTipa.SREBRNI, 5, 20001);
                        kupac.Tip = tip;
                        break;
                    }
                    else if (kupac.BrojSakupljenihBodova > 30000)
                    {
                        TipKorisnika tip = new TipKorisnika(ImeTipa.ZLATNI, 7, 30001);
                        kupac.Tip = tip;
                        break;
                    }
                }
            }
            HttpContext.Application["rezervacije"] = rezervacije;
            Podaci.AzurirajRezervaciju(rezervacije);
            HttpContext.Application["kupci"] = kupci;
            Podaci.AzururajKupca(kupci);
            return RedirectToAction("SveKarte");
        }

        public ActionResult OdustaniPrijeKupovine(string id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];


            foreach (var rez in rezervacije)
            {
                if (rez.karta.IdKarte.Equals(id) && rez.karta.Status.ToString().Equals("CEKANJE"))
                {
                    rezervacije.Remove(rez);
                    break;
                }
            }
            HttpContext.Application["rezervacije"] = rezervacije;
            return RedirectToAction("Index","Home");
        }

        public ActionResult Sortiraj(string sortirajPo,string opRas)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Rezervacija> rezKupca = (List<Rezervacija>)HttpContext.Application["rezervacijekupca"];
            List<Rezervacija> pomocna = new List<Rezervacija>();

            if (opRas.Equals(""))
            {
                opRas = "Rastuce";
            }

            switch (sortirajPo)
            {
                case "Ime_manifestacije":
                    if (opRas.Equals("Rastuce"))
                    {
                        pomocna = rezKupca.OrderBy(v => v.karta.ZaKojuManifestaciju.Naziv).ToList();
                        break;
                    }
                    else
                    {
                        pomocna = rezKupca.OrderByDescending(v => v.karta.ZaKojuManifestaciju.Naziv).ToList();
                        break;
                    }

                case "Cijena":
                    if (opRas.Equals("Rastuce"))
                    {
                        pomocna = rezKupca.OrderBy(v => v.karta.Cijena).ToList();
                        break;
                    }
                    else
                    {
                        pomocna = rezKupca.OrderByDescending(v => v.karta.Cijena).ToList();
                        break;
                    }
                case "Datum_odrzavanja":
                    if (opRas.Equals("Rastuce"))
                    {
                        pomocna = rezKupca.OrderBy(v => v.karta.DatumIvrijemeManifestacije).ToList();
                        break;
                    }
                    else
                    {
                        pomocna = rezKupca.OrderByDescending(v => v.karta.DatumIvrijemeManifestacije).ToList();
                        break;
                    }
                default:
                    pomocna = rezKupca;
                    break;
            }
            return View("SveKarte",pomocna);
;        }

        public ActionResult Pretrazi(string naziv, string cijenaOd,string cijenaDo,string datumOd, string datumDo)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Rezervacija> pomocna = new List<Rezervacija>();
            List<Rezervacija> pomocnaDatum = new List<Rezervacija>();
            List<Rezervacija> pomocnaCijena = new List<Rezervacija>();

            DateTime dateOd = DateTime.Now;
            DateTime dateDo = DateTime.Now;
            double cijena1 = 0;
            double cijena2 = 0;

            if (naziv.Equals("") && datumOd.Equals("") && datumDo.Equals("") && cijenaOd.Equals("") && cijenaDo.Equals(""))
            {
                foreach (Rezervacija rez in rezervacije)
                {
                    if (rez.karta.kupac.Id == kupac.Id)
                    {
                        pomocna.Add(rez);
                       
                    }
                }
                return View("SveKarte", pomocna);
            }

            if (!naziv.Equals("") && !datumOd.Equals("") && !datumDo.Equals("") && !cijenaOd.Equals("") && !cijenaDo.Equals(""))
            {
                try
                {
                    dateOd = DateTime.Parse(datumOd);
                    dateDo = DateTime.Parse(datumDo);
                }
                catch
                {
                    TempData["pretraga"]=$"Polje DATUM nije pravilno popunjeno!";
                    return View("SveKarte", pomocna);
                }

                cijena1 = double.Parse(cijenaOd);
                cijena2 = double.Parse(cijenaDo);

                foreach (Rezervacija rez in rezervacije)
                {
                    if (rez.karta.kupac.Id == kupac.Id)
                    {
                        if (rez.karta.DatumIvrijemeManifestacije >= dateOd && rez.karta.DatumIvrijemeManifestacije <= dateDo && rez.karta.ZaKojuManifestaciju.Naziv.ToUpper().Equals(naziv.ToUpper()) && rez.karta.Cijena > cijena1 && rez.karta.Cijena < cijena2)
                        {
                            pomocna.Add(rez);
                        }
                    }
                }

                return View("SveKarte", pomocna);
            }
            else
            {
                if (!datumOd.Equals("") && !datumDo.Equals(""))
                {
                    try
                    {
                        dateOd = DateTime.Parse(datumOd);
                        dateDo = DateTime.Parse(datumDo);
                    }
                    catch
                    {
                        TempData["pretraga"] = $"Polje DATUM nije pravilno popunjeno!";
                        return View("SveKarte", pomocna);
                    }

                    foreach (Rezervacija rez in rezervacije)
                    {
                        if (rez.karta.kupac.Id == kupac.Id)
                        {
                            if (rez.karta.DatumIvrijemeManifestacije >= dateOd && rez.karta.DatumIvrijemeManifestacije <= dateDo)
                            {
                                pomocnaDatum.Add(rez);
                            }
                        }
                    }


                }
                else
                {
                    if (!datumOd.Equals(""))
                    {
                        try
                        {
                            dateOd = DateTime.Parse(datumOd);
                        }
                        catch
                        {
                            TempData["pretraga"] = $"Polje DATUM nije pravilno popunjeno!";
                            return View("SveKarte", pomocna);
                        }

                        foreach (Rezervacija rez in rezervacije)
                        {
                            if (rez.karta.kupac.Id == kupac.Id)
                            {
                                if (rez.karta.DatumIvrijemeManifestacije >= dateOd)
                                {
                                    pomocnaDatum.Add(rez);
                                }
                            }
                        }

                        if (!naziv.Equals(""))
                        {
                            foreach (Rezervacija rez in pomocnaDatum)
                            {
                                if (rez.karta.kupac.Id == kupac.Id)
                                {
                                    if (rez.karta.ZaKojuManifestaciju.Naziv.ToUpper() == naziv.ToUpper())
                                    {
                                        pomocna.Add(rez);

                                    }
                                }
                            }


                        }
                        else
                        {
                            if (!cijenaOd.Equals("") && !cijenaDo.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);
                                cijena2 = double.Parse(cijenaDo);

                                foreach (Rezervacija rez in pomocnaDatum)
                                {
                                    if (rez.karta.kupac.Id == kupac.Id)
                                    {
                                        if (rez.karta.Cijena > cijena1 && rez.karta.Cijena < cijena2)
                                        {
                                            pomocnaCijena.Add(rez);

                                        }
                                    }
                                }
                                return View("SveKarte", pomocnaCijena);
                            }
                            else
                            {
                                if (!cijenaOd.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);

                                    foreach (Rezervacija rez in pomocnaDatum)
                                    {
                                        if (rez.karta.kupac.Id == kupac.Id)
                                        {
                                            if (rez.karta.Cijena > cijena1)
                                            {
                                                pomocnaCijena.Add(rez);

                                            }
                                        }
                                    }
                                    return View("SveKarte", pomocnaCijena);
                                }
                                else
                                {
                                    if (!cijenaDo.Equals(""))
                                    {
                                        cijena2 = double.Parse(cijenaDo);

                                        foreach (Rezervacija rez in pomocnaDatum)
                                        {
                                            if (rez.karta.kupac.Id == kupac.Id)
                                            {
                                                if (rez.karta.Cijena < cijena2)
                                                {
                                                    pomocnaCijena.Add(rez);

                                                }
                                            }
                                        }

                                        return View("SveKarte", pomocnaCijena);
                                    }
                                    else
                                    {

                                        return View("SveKarte", pomocnaDatum);
                                    }
                                }
                            }

                        }

                        if (!cijenaOd.Equals("") && !cijenaDo.Equals(""))
                        {
                            cijena1 = double.Parse(cijenaOd);
                            cijena2 = double.Parse(cijenaDo);

                            foreach (Rezervacija rez in pomocna)
                            {
                                if (rez.karta.kupac.Id == kupac.Id)
                                {
                                    if (rez.karta.Cijena > cijena1 && rez.karta.Cijena < cijena2)
                                    {
                                        pomocnaCijena.Add(rez);

                                    }
                                }
                            }
                            return View("SveKarte", pomocnaCijena);
                        }
                        else
                        {
                            if (!cijenaOd.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);

                                foreach (Rezervacija rez in pomocna)
                                {
                                    if (rez.karta.kupac.Id == kupac.Id)
                                    {
                                        if (rez.karta.Cijena > cijena1)
                                        {
                                            pomocnaCijena.Add(rez);

                                        }
                                    }
                                }
                                return View("SveKarte", pomocnaCijena);
                            }
                            else
                            {
                                if (!cijenaDo.Equals(""))
                                {
                                    cijena2 = double.Parse(cijenaDo);

                                    foreach (Rezervacija rez in pomocna)
                                    {
                                        if (rez.karta.kupac.Id == kupac.Id)
                                        {
                                            if (rez.karta.Cijena < cijena2)
                                            {
                                                pomocnaCijena.Add(rez);

                                            }
                                        }
                                    }
                                    return View("SveKarte", pomocnaCijena);
                                }
                                else
                                {
                                    return View("SveKarte", pomocna);
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        if (!datumDo.Equals(""))
                        {
                            try
                            {
                                dateDo = DateTime.Parse(datumDo);
                            }
                            catch
                            {
                                TempData["pretraga"] = $"Polje DATUM nije pravilno popunjeno!";
                                return View("SveKarte", pomocna);

                            }
                            foreach (Rezervacija rez in rezervacije)
                            {
                                if (rez.karta.kupac.Id == kupac.Id)
                                {
                                    if (rez.karta.DatumIvrijemeManifestacije <= dateDo)
                                    {
                                            pomocnaDatum.Add(rez);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!naziv.Equals(""))
                            {
                                foreach (Rezervacija rez in rezervacije)
                                {
                                    if (rez.karta.kupac.Id == kupac.Id)
                                    {
                                        if (rez.karta.ZaKojuManifestaciju.Naziv.ToUpper() == naziv.ToUpper())
                                        {
                                            pomocna.Add(rez);

                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (!cijenaOd.Equals("") && !cijenaDo.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);
                                    cijena2 = double.Parse(cijenaDo);

                                    foreach (Rezervacija rez in rezervacije)
                                    {
                                        if (rez.karta.kupac.Id == kupac.Id)
                                        {
                                            if (rez.karta.Cijena > cijena1 && rez.karta.Cijena < cijena2)
                                            {
                                                pomocnaCijena.Add(rez);

                                            }
                                        }
                                    }
                                    return View("SveKarte", pomocnaCijena);
                                }
                                else
                                {
                                    if (!cijenaOd.Equals(""))
                                    {
                                        cijena1 = double.Parse(cijenaOd);

                                        foreach (Rezervacija rez in rezervacije)
                                        {
                                            if (rez.karta.kupac.Id == kupac.Id)
                                            {
                                                if (rez.karta.Cijena > cijena1)
                                                {
                                                    pomocnaCijena.Add(rez);

                                                }
                                            }
                                        }
                                        return View("SveKarte", pomocnaCijena);
                                    }
                                    else
                                    {
                                        if (!cijenaDo.Equals(""))
                                        {
                                            cijena2 = double.Parse(cijenaDo);

                                            foreach (Rezervacija rez in rezervacije)
                                            {
                                                if (rez.karta.kupac.Id == kupac.Id)
                                                {
                                                    if (rez.karta.Cijena < cijena2)
                                                    {
                                                        pomocnaCijena.Add(rez);

                                                    }
                                                }
                                            }
                                            return View("SveKarte", pomocnaCijena);
                                        }
                                        else
                                        {
                                            return View("SveKarte", rezervacije);
                                        }
                                    }
                                }

                            }
                            if (!cijenaOd.Equals("") && !cijenaDo.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);
                                cijena2 = double.Parse(cijenaDo);

                                foreach (Rezervacija rez in pomocna)
                                {
                                    if (rez.karta.kupac.Id == kupac.Id)
                                    {
                                        if (rez.karta.Cijena > cijena1 && rez.karta.Cijena < cijena2)
                                        {
                                            pomocnaCijena.Add(rez);

                                        }
                                    }
                                }
                                return View("SveKarte", pomocnaCijena);
                            }
                            else
                            {
                                if (!cijenaOd.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);

                                    foreach (Rezervacija rez in pomocna)
                                    {
                                        if (rez.karta.kupac.Id == kupac.Id)
                                        {
                                            if (rez.karta.Cijena > cijena1)
                                            {
                                                pomocnaCijena.Add(rez);

                                            }
                                        }
                                    }
                                    return View("SveKarte", pomocnaCijena);
                                }
                                else
                                {
                                    if (!cijenaDo.Equals(""))
                                    {
                                        cijena2 = double.Parse(cijenaDo);

                                        foreach (Rezervacija rez in pomocna)
                                        {
                                            if (rez.karta.kupac.Id == kupac.Id)
                                            {
                                                if (rez.karta.Cijena < cijena2)
                                                {
                                                    pomocnaCijena.Add(rez);

                                                }
                                            }
                                        }
                                        return View("SveKarte", pomocnaCijena);
                                    }
                                    else
                                    {
                                        return View("SveKarte", pomocna);
                                    }
                                    
                                }
                            }


                        }

                        if (!naziv.Equals(""))
                        {
                            foreach (Rezervacija rez in pomocnaDatum)
                            {
                                if (rez.karta.kupac.Id == kupac.Id)
                                {
                                    if (rez.karta.ZaKojuManifestaciju.Naziv.ToUpper() == naziv.ToUpper())
                                    {
                                        pomocna.Add(rez);

                                    }
                                }
                            }
                        }
                        else
                        {
                            if (!cijenaOd.Equals("") && !cijenaDo.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);
                                cijena2 = double.Parse(cijenaDo);

                                foreach (Rezervacija rez in pomocnaDatum)
                                {
                                    if (rez.karta.kupac.Id == kupac.Id)
                                    {
                                        if (rez.karta.Cijena > cijena1 && rez.karta.Cijena < cijena2)
                                        {
                                            pomocnaCijena.Add(rez);

                                        }
                                    }
                                }
                                return View("SveKarte", pomocnaCijena);
                            }
                            else
                            {
                                if (!cijenaOd.Equals(""))
                                {
                                    cijena1 = double.Parse(cijenaOd);

                                    foreach (Rezervacija rez in pomocnaDatum)
                                    {
                                        if (rez.karta.kupac.Id == kupac.Id)
                                        {
                                            if (rez.karta.Cijena > cijena1)
                                            {
                                                pomocnaCijena.Add(rez);

                                            }
                                        }
                                    }

                                    return View("SveKarte", pomocnaCijena);
                                }
                                else
                                {
                                    if (!cijenaDo.Equals(""))
                                    {
                                        cijena2 = double.Parse(cijenaDo);

                                        foreach (Rezervacija rez in pomocnaDatum)
                                        {
                                            if (rez.karta.kupac.Id == kupac.Id)
                                            {
                                                if (rez.karta.Cijena < cijena2)
                                                {
                                                    pomocnaCijena.Add(rez);

                                                }
                                            }
                                        }

                                        return View("SveKarte", pomocnaCijena);
                                    }
                                    else
                                    {

                                        return View("SveKarte", pomocnaDatum);
                                    }
                                }
                            }

                        }

                        if (!cijenaOd.Equals("") && !cijenaDo.Equals(""))
                        {
                            cijena1 = double.Parse(cijenaOd);
                            cijena2 = double.Parse(cijenaDo);

                            foreach (Rezervacija rez in pomocna)
                            {
                                if (rez.karta.kupac.Id == kupac.Id)
                                {
                                    if (rez.karta.Cijena > cijena1 && rez.karta.Cijena < cijena2)
                                    {
                                        pomocnaCijena.Add(rez);

                                    }
                                }
                            }
                            return View("SveKarte", pomocnaCijena);
                        }
                        else
                        {
                            if (!cijenaOd.Equals(""))
                            {
                                cijena1 = double.Parse(cijenaOd);

                                foreach (Rezervacija rez in pomocna)
                                {
                                    if (rez.karta.kupac.Id == kupac.Id)
                                    {
                                        if (rez.karta.Cijena > cijena1)
                                        {
                                            pomocnaCijena.Add(rez);

                                        }
                                    }
                                }
                                return View("SveKarte", pomocnaCijena);
                            }
                            else
                            {
                                if (!cijenaDo.Equals(""))
                                {
                                    cijena2 = double.Parse(cijenaDo);

                                    foreach (Rezervacija rez in pomocna)
                                    {
                                        if (rez.karta.kupac.Id == kupac.Id)
                                        {
                                            if (rez.karta.Cijena < cijena2)
                                            {
                                                pomocnaCijena.Add(rez);

                                            }
                                        }
                                    }
                                    return View("SveKarte", pomocnaCijena);
                                }
                                else
                                {
                                    return View("SveKarte", pomocna);
                                }
                            }
                        }

                    }
                }

               
                if (!naziv.Equals(""))
                {
                    foreach (Rezervacija rez in pomocnaDatum)
                    {
                        if (rez.karta.kupac.Id == kupac.Id)
                        {
                            if (rez.karta.ZaKojuManifestaciju.Naziv.ToUpper() == naziv.ToUpper())
                            {
                                    pomocna.Add(rez);
                                
                            }
                        }
                    }
                }
                else
                {

                }

                if (!cijenaOd.Equals("") && !cijenaDo.Equals(""))
                {
                    cijena1 = double.Parse(cijenaOd);
                    cijena2 = double.Parse(cijenaDo);

                    foreach (Rezervacija rez in pomocna)
                    {
                        if (rez.karta.kupac.Id == kupac.Id)
                        {
                            if (rez.karta.Cijena > cijena1 && rez.karta.Cijena < cijena2)
                            {
                                    pomocnaCijena.Add(rez);
                                
                            }
                        }
                    }
                    return View("SveKarte", pomocnaCijena);
                }
                else
                {
                    if (!cijenaOd.Equals(""))
                    {
                        cijena1 = double.Parse(cijenaOd);

                        foreach (Rezervacija rez in pomocna)
                        {
                            if (rez.karta.kupac.Id == kupac.Id)
                            {
                                if (rez.karta.Cijena > cijena1)
                                {
                                    pomocnaCijena.Add(rez);

                                }
                            }
                        }
                        return View("SveKarte", pomocnaCijena);
                    }
                    else
                    {
                        if (!cijenaDo.Equals(""))
                        {
                            cijena2 = double.Parse(cijenaDo);

                            foreach (Rezervacija rez in pomocna)
                            {
                                if (rez.karta.kupac.Id == kupac.Id)
                                {
                                    if (rez.karta.Cijena < cijena2)
                                    {
                                        pomocnaCijena.Add(rez);

                                    }
                                }
                            }

                            return View("SveKarte", pomocnaCijena);
                        }
                        else
                        {
                            return View("SveKarte", pomocna);
                        }
                    }
                }
                

                }
            
        }

        public ActionResult Filtriraj(string vip, string regular, string fanPit, string rezervisana, string odustanak)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            List<Rezervacija> rezKupca = (List<Rezervacija>)HttpContext.Application["rezervacijekupca"];
            List<Rezervacija> pomocna = new List<Rezervacija>();

            if (vip == null && regular == null && fanPit == null && rezervisana == null && odustanak == null)
            {
                pomocna = rezKupca;
                return View("SveKarte", pomocna);
            }

            if (vip != null && regular != null && fanPit != null && rezervisana != null && odustanak != null)
            {
                pomocna = rezKupca;
                return View("SveKarte", pomocna);
            }
            else
            {
                if (vip == null &&  regular == null && fanPit==null)
                {
                    if (rezervisana != null && odustanak != null)
                    {
                        foreach (var p in rezKupca)
                        {
                            if (!pomocna.Contains(p))
                            {
                                pomocna.Add(p);
                            }
                        }
                    }
                    else if (rezervisana != null)
                    {
                        foreach (var p in rezKupca)
                        {
                            if (!pomocna.Contains(p) && p.karta.Status.ToString().Equals("REZERVISANA"))
                            {
                                pomocna.Add(p);
                            }
                        }
                    }
                    else if (odustanak != null)
                    {
                        foreach (var p in rezKupca)
                        {
                            if (!pomocna.Contains(p) && p.karta.Status.ToString().Equals("ODUSTANAK"))
                            {
                                pomocna.Add(p);
                            }
                        }
                    }
                }
                else
                {
                    if (vip != null)
                    {
                        if (rezervisana != null && odustanak != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("VIP"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else if (rezervisana != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("VIP") && p.karta.Status.ToString().Equals("REZERVISANA"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else if (odustanak != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("VIP") && p.karta.Status.ToString().Equals("ODUSTANAK"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("VIP"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                    }

                    if (regular != null)
                    {
                        if (rezervisana != null && odustanak != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("REGULAR"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else if (rezervisana != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("REGULAR") && p.karta.Status.ToString().Equals("REZERVISANA"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else if (odustanak != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("REGULAR") && p.karta.Status.ToString().Equals("ODUSTANAK"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("REGULAR"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                    }

                    if (fanPit != null)
                    {
                        if (rezervisana != null && odustanak != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("FAN_PIT"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else if (rezervisana != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("FAN_PIT") && p.karta.Status.ToString().Equals("REZERVISANA"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else if (odustanak != null)
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("FAN_PIT") && p.karta.Status.ToString().Equals("ODUSTANAK"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                        else
                        {
                            foreach (var p in rezKupca)
                            {
                                if (p.karta.Tip.ToString().Equals("FAN_PIT"))
                                {
                                    pomocna.Add(p);
                                }
                            }
                        }
                    }


                }
                return View("SveKarte", pomocna);
            }

            #region DobroFiltriranje
            /*
            List<Rezervacija> pomocna1 = new List<Rezervacija>();
            List<Rezervacija> pomocna2 = new List<Rezervacija>();
            List<Rezervacija> pomocna3 = new List<Rezervacija>();
            if (rezervisana!=null && odustanak != null)
            {
                if (vip != null)
                {
                    foreach(var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("VIP"))
                        {
                            pomocna1.Add(p);
                        }
                    }
                }
                if (regular != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("REGULAR"))
                        {
                            pomocna1.Add(p);
                        }
                    }
                }
                if (fanPit != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("FAN_PIT"))
                        {
                            pomocna1.Add(p);
                        }
                    }
                }
                if (vip == null && regular == null && fanPit == null)
                {
                    pomocna1 = rezKupca;
                }
                return View("SveKarte", pomocna1);

            }
            else if(rezervisana!=null)
            {
                if (vip != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("VIP") && p.karta.Status.ToString().Equals("REZERVISANA"))
                        {
                            pomocna2.Add(p);
                        }
                    }
                }
                if (regular != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("REGULAR") && p.karta.Status.ToString().Equals("REZERVISANA"))
                        {
                            pomocna2.Add(p);
                        }
                    }
                }
                if (fanPit != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("FAN_PIT") && p.karta.Status.ToString().Equals("REZERVISANA"))
                        {
                            pomocna2.Add(p);
                        }
                    }
                }
                if (vip == null && regular == null && fanPit == null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Status.ToString().Equals("REZERVISANA"))
                        {
                            pomocna2.Add(p);
                        }
                    }
                }
                return View("SveKarte", pomocna2);

            }
            else if (odustanak != null)
            {
                if (vip != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("VIP") && p.karta.Status.ToString().Equals("ODUSTANAK"))
                        {
                            pomocna3.Add(p);
                        }
                    }
                }
                if (regular != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("REGULAR") && p.karta.Status.ToString().Equals("ODUSTANAK"))
                        {
                            pomocna3.Add(p);
                        }
                    }
                }
                if (fanPit != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("FAN_PIT") && p.karta.Status.ToString().Equals("ODUSTANAK"))
                        {
                            pomocna3.Add(p);
                        }
                    }
                }
                if (vip == null && regular == null && fanPit == null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Status.ToString().Equals("ODUSTANAK"))
                        {
                            pomocna3.Add(p);
                        }
                    }
                }
                return View("SveKarte", pomocna3);

            }
            else
            {
                if (vip != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("VIP"))
                        {
                            pomocna.Add(p);
                        }
                    }
                }
                if (regular != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("REGULAR"))
                        {
                            pomocna.Add(p);
                        }
                    }
                }
                if (fanPit != null)
                {
                    foreach (var p in rezKupca)
                    {
                        if (p.karta.Tip.ToString().Equals("FAN_PIT"))
                        {
                            pomocna.Add(p);
                        }
                    }
                }
                if (vip == null && regular == null && fanPit == null)
                {
                    pomocna = rezKupca;
                }
                return View("SveKarte", pomocna);
                }
                */
        
                #endregion
        }

        public ActionResult Komentar(string id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];

            foreach(var r in rezervacije)
            {
                if (r.id.ToString()==id)
                {
                    return View(r);
                }
            }

            return View();
        }

        public ActionResult OstaviKomentar(string komentar, string ocjena,string id)
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            List<Rezervacija> rezervacije = (List<Rezervacija>)HttpContext.Application["rezervacije"];
            List<Komentar> komentari = (List<Komentar>)HttpContext.Application["komentari"];
            List<Manifestacija> manifestacije = (List<Manifestacija>)HttpContext.Application["manifestacije"];
            List<Korisnik> kupci = (List<Korisnik>)HttpContext.Application["kupci"];
            Manifestacija manifestacija = new Manifestacija();
            Rezervacija rezervacija = new Rezervacija();

            if (komentar.Equals("") && ocjena.Equals(""))
            {
                TempData["komentar"] = $"Sva polja moraju biti popunjena";
                foreach(var r in rezervacije)
                {
                    if (r.id.ToString().Equals(id))
                    {
                        return View("Komentar", r);
                    }
                }
            }
            else
            {
                if (komentar.Equals(""))
                {
                    TempData["komentar"] = $"Polje 'Komenatr' mora biti popunjeno";
                    foreach (var r in rezervacije)
                    {
                        if (r.id.ToString().Equals(id))
                        {
                            return View("Komentar", r);
                        }
                    }
                }
                if (ocjena.Equals(""))
                {
                    TempData["komentar"] = $"Polje 'Ocjena' mora biti popunjeno";
                    foreach (var r in rezervacije)
                    {
                        if (r.id.ToString().Equals(id))
                        {
                            return View("Komentar", r);
                        }
                    }
                }

            }
            
            foreach(var rez in rezervacije)
            {
                if (rez.id.ToString()==id)
                {
                    rezervacija = rez;
                    break;
                }

            }
            foreach(var m in manifestacije)
            {
                if (m.Id.ToString() == rezervacija.karta.ZaKojuManifestaciju.Id.ToString())
                {
                    manifestacija = m;
                }
            }

            Komentar k = new Komentar(komentari.Count+1, kupac, manifestacija, komentar, int.Parse(ocjena),TipKomentara.CEKANJE);
            Komentar k1 = new Komentar(k.Id, kupac.Id, manifestacija.Id, k.TekstKomentara, k.Ocjena, k.Tip);
            komentari.Add(k1);
            HttpContext.Application["komentari"] = komentari;
            Podaci.SacuvajKomentar(k);

            return RedirectToAction("RezervisaneKarte");
        }
        public ActionResult Odjava()
        {
            Korisnik kupac = (Korisnik)Session["kupci"];
            Korisnik admin = (Korisnik)Session["admin"];
            Korisnik prodavac = (Korisnik)Session["prodavci"];

            if (kupac == null)
            {
                if (admin == null)
                {
                    if (prodavac == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Prodavac");
                    }
                }
                else
                {
                    return RedirectToAction("NoveManifestacije", "Administrator");
                }
            }
            Session["kupci"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}