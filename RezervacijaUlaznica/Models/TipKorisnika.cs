﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.Models
{
    public class TipKorisnika
    {
        public ImeTipa Ime { get; set; }
        public int Popust { get; set; }
        public double BrojBodova { get; set; }

        public TipKorisnika(ImeTipa ime, int popust, double brojBodova)
        {
            Ime = ime;
            Popust = popust;
            BrojBodova = brojBodova;
        }

        public TipKorisnika()
        {
        }
    }
}