﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.Models
{
    public class Korisnik
    {
        [Required]
        public string KorisnickoIme { get; set; }
        [Required]
        public string Lozinka { get; set; }
        [Required]
        public string Ime { get; set; }
        [Required]
        public string Prezime { get; set; }
        [Required]
        public PolTip Pol { get; set; }
        [Required]
        public DateTime DatumRodjenja { get; set; }
        public UlogaKorisnika Uloga { get; set; }
        public List<Karta> SveKarte { get; set; } //Ako je korisnik kupac
        public List<Manifestacija> SveManifestacije { get; set; } //Ako je korisnik Prodavac
        public double BrojSakupljenihBodova { get; set; } //Ako je korisnik kupac
        public TipKorisnika Tip { get; set; }
        public bool Obrisan { get; set; }
        public int Id { get; set; }

        public Korisnik(string korisnickoIme, string lozinka, string ime, string prezime, PolTip pol, DateTime datumRodjenja, UlogaKorisnika uloga, double brojSakupljenihBodova, TipKorisnika tip,bool obrisan,int id)
        {
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            Ime = ime;
            Prezime = prezime;
            Pol = pol;
            DatumRodjenja = datumRodjenja;
            Uloga = uloga;
            BrojSakupljenihBodova = brojSakupljenihBodova;
            Tip = tip;
            Obrisan = obrisan;
            Id = id;
        }

        public Korisnik()
        {
        }

        public override bool Equals(object obj)
        {
            return ((Korisnik)obj).KorisnickoIme.Equals(this.KorisnickoIme);
        }
    }
}