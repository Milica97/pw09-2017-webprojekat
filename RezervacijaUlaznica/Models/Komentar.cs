﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.Models
{
    public class Komentar
    {
        public int Id { get; set; }
        public Korisnik KupacKarte { get; set; }
        public int IdKupca { get; set; }
        public Manifestacija Manifestacija { get; set; }
        public int IdManifestacije { get; set; }
        public string TekstKomentara { get; set; }
        public int Ocjena { get; set; }
        public TipKomentara Tip { get; set; }

        public Komentar(int id,Korisnik kupacKarte, Manifestacija manifestacija, string tekstKomentara, int ocjena,TipKomentara tip)
        {
            Id = id;
            KupacKarte = kupacKarte;
            Manifestacija = manifestacija;
            TekstKomentara = tekstKomentara;
            Ocjena = ocjena;
            Tip = tip;
        }

        public Komentar()
        {
        }

        public Komentar(int id,int idKupca, int idManifestacije, string tekstKomentara, int ocjena,TipKomentara tip)
        {
            Id = id;
            IdKupca = idKupca;
            IdManifestacije = idManifestacije;
            TekstKomentara = tekstKomentara;
            Ocjena = ocjena;
            Tip = tip;
        }
    }
}