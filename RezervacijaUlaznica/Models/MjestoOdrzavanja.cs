﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.Models
{
    public class MjestoOdrzavanja
    {
        public string Adresa { get; set; }
        public string Ulica { get; set; }
        public int Broj { get; set; }
        public string Grad { get; set; }
        public long PostanskiBroj { get; set; }

        public MjestoOdrzavanja(string ulica, int broj, string grad, long postanskiBroj)
        {
            Ulica = ulica;
            Broj = broj;
            Grad = grad;
            PostanskiBroj = postanskiBroj;
            Adresa = ulica +" "+ Broj.ToString() + "," + Grad + "," + PostanskiBroj.ToString();
        }

        public MjestoOdrzavanja()
        {
        }
    }
}