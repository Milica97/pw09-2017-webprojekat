﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.Models
{
    public class Rezervacija
    {
        public Karta karta { get; set; }
        public int kolicina { get; set; }
        public int id { get; set; }

        public Rezervacija( Karta karta,int kolicina,int id)
        {
            this.karta = karta;
            this.kolicina = kolicina;
            this.id = id;
        }

        public Rezervacija()
        {
        }
    }
}