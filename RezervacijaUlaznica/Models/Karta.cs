﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.Models
{
    public class Karta
    {
        //10 karaktera
        public string IdKarte { get; set; }
        public Manifestacija ZaKojuManifestaciju { get; set; }
        public DateTime DatumIvrijemeManifestacije { get; set; }
        public double Cijena { get; set; }
        public Korisnik kupac { get; set; }
        public StatusKarte Status { get; set; }
        public TipKarte Tip { get; set; }

        public Karta(string idKarte, Manifestacija zaKojuManifestaciju, DateTime datumIvrijemeManifestacije, double cijena, Korisnik kupac, StatusKarte status, TipKarte tip)
        {
            IdKarte = idKarte;
            ZaKojuManifestaciju = zaKojuManifestaciju;
            DatumIvrijemeManifestacije = datumIvrijemeManifestacije;
            Cijena = cijena;
            this.kupac = kupac;
            Status = status;
            Tip = tip;
        }

        public Karta()
        {
        }

        public Karta(string idKarte)
        {
            IdKarte = idKarte;
        }
    }
}