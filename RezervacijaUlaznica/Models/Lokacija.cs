﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.Models
{
    public class Lokacija
    {
        public double GeografskaDuzina { get; set; }
        public double GeografskaSirina { get; set; }
        public MjestoOdrzavanja Mjesto { get; set; }

        public Lokacija(double geografskaDuzina, double geografskaSirina, MjestoOdrzavanja mjesto)
        {
            GeografskaDuzina = geografskaDuzina;
            GeografskaSirina = geografskaSirina;
            Mjesto = mjesto;
        }

        public Lokacija()
        {
        }
    }
}