﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RezervacijaUlaznica.Models
{
    public class Manifestacija
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public TipManifestacije Tip { get; set; }
        public int BrojMijestaRegular { get; set; }
        public int BrojMijestaVip { get; set; }
        public int BrojMijestaFanPit { get; set; }
        public int BrojMijesta { get; set; }
        public DateTime DatumIVrijemeOdrzavanja { get; set; }
        public double CijenaRegularKarte { get; set; }
        public StatusManifestacije Status { get; set; }
        //[RegularExpression(@"Ulica broj,grad,postanski")]
        public MjestoOdrzavanja Mjesto { get; set; }
        public bool Zavrsena { get; set; }
        public int IdProdavca { get; set; }
        public int BrSlobodnihRegular { get; set; }
        public int BrSlobodnihVip { get; set; }
        public int BrSlobodnihFanPit { get; set; }
        public double ProsjecnaOcjena { get; set; }

        public string Poster { get; set; }


        public Manifestacija(int id,string naziv, TipManifestacije tip, int brojMijestaRegular,int brojMijestaVip,int brojMijestaFanPit,int brojMijesta,DateTime datumIVrijemeOdrzavanja, double cijenaRegularKarte, StatusManifestacije status,string img, MjestoOdrzavanja mjesto ,bool zavrsena,int idProdavca,double prosjecnaOcjena, int brSlobodnihRegular, int brSlobodnihVip, int brSlobodnihFanPit)
        {
            Id = id;
            Naziv = naziv;
            Tip = tip;
            BrojMijestaRegular = brojMijestaRegular;
            BrojMijestaFanPit = brojMijestaFanPit;
            BrojMijestaVip = brojMijestaVip;
            BrojMijesta = brojMijesta;
            BrSlobodnihRegular = brSlobodnihRegular;
            BrSlobodnihVip = brSlobodnihVip;
            BrSlobodnihFanPit = brSlobodnihFanPit;
            DatumIVrijemeOdrzavanja = datumIVrijemeOdrzavanja;
            CijenaRegularKarte = cijenaRegularKarte;
            Status = status;
            Poster = img;
            Mjesto = mjesto;
            Zavrsena = zavrsena;
            IdProdavca = idProdavca;
            ProsjecnaOcjena = prosjecnaOcjena;
        }

        public Manifestacija(int id)
        {
            Id = id;
        }

        public Manifestacija()
        {
        }
        




    }
}