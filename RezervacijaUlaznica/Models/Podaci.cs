﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace RezervacijaUlaznica.Models
{
    public class Podaci
    {
        public static List<Manifestacija> ProcitajManifestacije(string path)
        {
            List<Manifestacija> manifestacije = new List<Manifestacija>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                string[] tokens1 = tokens[11].Split(',');
                string[] tokens2 = tokens1[0].Split(' ');
                MjestoOdrzavanja mjesto = new MjestoOdrzavanja(tokens2[0],int.Parse(tokens2[1]),tokens1[1],long.Parse(tokens1[2]));
                Manifestacija m = new Manifestacija(int.Parse(tokens[0]),tokens[1],(TipManifestacije)Enum.Parse(typeof(TipManifestacije),tokens[2]),int.Parse(tokens[3]), int.Parse(tokens[4]), int.Parse(tokens[5]), int.Parse(tokens[6]),DateTime.Parse(tokens[7]),double.Parse(tokens[8]),(StatusManifestacije)Enum.Parse(typeof(StatusManifestacije),tokens[9]),tokens[10],mjesto,bool.Parse(tokens[12]),int.Parse(tokens[13]),double.Parse(tokens[14]), int.Parse(tokens[15]), int.Parse(tokens[16]), int.Parse(tokens[17]));
                manifestacije.Add(m);
            }

            sr.Close();
            stream.Close();

            return manifestacije;
        }
        public static List<Korisnik> ProcitajKupce(string path)
        {
            List<Korisnik> kupci = new List<Korisnik>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                TipKorisnika tip = new TipKorisnika((ImeTipa)Enum.Parse(typeof(ImeTipa), tokens[9]), int.Parse(tokens[10]), double.Parse(tokens[11]));
                Korisnik k = new Korisnik(tokens[1], tokens[2], tokens[3], tokens[4], (PolTip)Enum.Parse(typeof(PolTip), tokens[5]), DateTime.Parse(tokens[6]), (UlogaKorisnika)Enum.Parse(typeof(UlogaKorisnika), tokens[7]),double.Parse(tokens[8]),tip,bool.Parse(tokens[12]), int.Parse(tokens[0]));
                kupci.Add(k);
            }
            sr.Close();
            stream.Close();

            return kupci;
        }
        public static List<Korisnik> ProcitajProdavce(string path)
        {
            List<Korisnik> prodavci = new List<Korisnik>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                TipKorisnika tip = new TipKorisnika();
                Korisnik k = new Korisnik(tokens[1], tokens[2], tokens[3], tokens[4], (PolTip)Enum.Parse(typeof(PolTip), tokens[5]), DateTime.Parse(tokens[6]), (UlogaKorisnika)Enum.Parse(typeof(UlogaKorisnika), tokens[7]), double.Parse(tokens[8]),tip,bool.Parse(tokens[9]), int.Parse(tokens[0]));
                prodavci.Add(k);
            }
            sr.Close();
            stream.Close();

            return prodavci;
        }
        public static List<Korisnik> ProcitajAdministratore(string path)
        {
            List<Korisnik> administratori = new List<Korisnik>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                TipKorisnika tip = new TipKorisnika();
                Korisnik k = new Korisnik(tokens[1], tokens[2], tokens[3], tokens[4], (PolTip)Enum.Parse(typeof(PolTip), tokens[5]), DateTime.Parse(tokens[6]), (UlogaKorisnika)Enum.Parse(typeof(UlogaKorisnika), tokens[7]), double.Parse(tokens[8]),tip,false,int.Parse(tokens[0]));
                administratori.Add(k);
            }
            sr.Close();
            stream.Close();

            return administratori;
        }
        public static List<Rezervacija> ProcitajRezervacije(string path)
        {
            List<Rezervacija> rezervacije = new List<Rezervacija>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                string[] tokens1 = tokens[12].Split(',');
                string[] tokens2 = tokens1[0].Split(' ');
                MjestoOdrzavanja mjesto = new MjestoOdrzavanja(tokens2[0], int.Parse(tokens2[1]), tokens1[1], long.Parse(tokens1[2]));
                Manifestacija m = new Manifestacija(int.Parse(tokens[1]), tokens[2], (TipManifestacije)Enum.Parse(typeof(TipManifestacije), tokens[3]), int.Parse(tokens[4]), int.Parse(tokens[5]), int.Parse(tokens[6]), int.Parse(tokens[7]), DateTime.Parse(tokens[8]), double.Parse(tokens[9]), (StatusManifestacije)Enum.Parse(typeof(StatusManifestacije), tokens[10]),tokens[11], mjesto, bool.Parse(tokens[13]), int.Parse(tokens[14]),double.Parse(tokens[33]), int.Parse(tokens[34]), int.Parse(tokens[35]), int.Parse(tokens[36]));
                TipKorisnika tip = new TipKorisnika((ImeTipa)Enum.Parse(typeof(ImeTipa), tokens[25]), int.Parse(tokens[26]), double.Parse(tokens[27]));
                Korisnik k = new Korisnik(tokens[17], tokens[18], tokens[19], tokens[20], (PolTip)Enum.Parse(typeof(PolTip), tokens[21]), DateTime.Parse(tokens[22]), (UlogaKorisnika)Enum.Parse(typeof(UlogaKorisnika), tokens[23]), double.Parse(tokens[24]), tip, bool.Parse(tokens[28]), int.Parse(tokens[16]));
                
                Karta karta = new Karta(tokens[0], m, m.DatumIVrijemeOdrzavanja, double.Parse(tokens[15]), k, (StatusKarte)Enum.Parse(typeof(StatusKarte), tokens[29]), (TipKarte)Enum.Parse(typeof(TipKarte), tokens[30]));
                int kolicina = int.Parse(tokens[31]);
                Rezervacija rez = new Rezervacija(karta, kolicina, int.Parse(tokens[32]));
                rezervacije.Add(rez);
            }
            sr.Close();
            stream.Close();

            return rezervacije;
        }
        public static List<Komentar> ProcitajKomentare(string path)
        {
            List<Komentar> komentari = new List<Komentar>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                Komentar k = new Komentar(int.Parse(tokens[0]),int.Parse(tokens[1]),int.Parse(tokens[2]),tokens[3],int.Parse(tokens[4]),(TipKomentara)Enum.Parse(typeof(TipKomentara),tokens[5]));
                komentari.Add(k);
            }

            sr.Close();
            stream.Close();

            return komentari;

        }


        public static void SacuvajManifestacije(Manifestacija manifestacija)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Manifestacije.txt");
            FileStream stream = new FileStream(path, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            int id = manifestacija.Id;
            string naziv = manifestacija.Naziv;
            TipManifestacije tip = manifestacija.Tip;
            int brojMjestaRegular = manifestacija.BrojMijestaRegular;
            int Vip = manifestacija.BrojMijestaVip;
            int FanPit = manifestacija.BrojMijestaFanPit;
            int ukupno = manifestacija.BrojMijesta;
            int brSlobodnihRegular = manifestacija.BrSlobodnihRegular;
            int brSlobodnihVip = manifestacija.BrSlobodnihVip;
            int brSlobodnihFanPit = manifestacija.BrSlobodnihFanPit;
            DateTime datumiVrijeme = manifestacija.DatumIVrijemeOdrzavanja;
            double cijena = manifestacija.CijenaRegularKarte;
            StatusManifestacije status = manifestacija.Status;
            MjestoOdrzavanja mjesto = manifestacija.Mjesto;
            bool zavrsena = manifestacija.Zavrsena;
            int idProdavca = manifestacija.IdProdavca;
            double prosjecnaOcjena = manifestacija.ProsjecnaOcjena;

            string img = manifestacija.Poster;

            string upisi = id + ";" + naziv + ";" + tip + ";" + brojMjestaRegular + ";" + Vip + ";" + FanPit + ";" + ukupno + ";" + datumiVrijeme + ";" + cijena + ";" + status + ";"+ img + ";" + mjesto.Adresa + ";" + zavrsena + ";" + idProdavca + ";" + prosjecnaOcjena + ";" + brSlobodnihRegular + ";" + brSlobodnihVip + ";" + brSlobodnihFanPit;
            sw.WriteLine(upisi);


            sw.Close();
            stream.Close();
        }
        public static void SacuvajKupce(Korisnik kupac)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Kupci.txt");
            FileStream stream = new FileStream(path, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            int id = kupac.Id;
            string korisnickoIme = kupac.KorisnickoIme;
            string lozinka = kupac.Lozinka;
            string ime = kupac.Ime;
            string prezime = kupac.Prezime;
            PolTip pol = kupac.Pol;
            DateTime datumRodjenja = kupac.DatumRodjenja;
            UlogaKorisnika uloga = UlogaKorisnika.KUPAC;
            double BrojBodova = kupac.BrojSakupljenihBodova;
            TipKorisnika tip = kupac.Tip ;
            bool obrisan = kupac.Obrisan;

            string upisi = id + ";" + korisnickoIme + ";" + lozinka + ";" + ime + ";" + prezime + ";" + pol + ";" + datumRodjenja + ";" + uloga + ";" + BrojBodova + ";" + tip.Ime + ";" + tip.Popust + ";" + tip.BrojBodova + ";" + obrisan;
            sw.WriteLine(upisi);


            sw.Close();
            stream.Close();
        }
        public static void SacuvajProdavce(Korisnik prodavac)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Prodavci.txt");
            FileStream stream = new FileStream(path, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            int id = prodavac.Id;
            string korisnickoIme = prodavac.KorisnickoIme;
            string lozinka = prodavac.Lozinka;
            string ime = prodavac.Ime;
            string prezime = prodavac.Prezime;
            PolTip pol = prodavac.Pol;
            DateTime datumRodjenja = prodavac.DatumRodjenja;
            UlogaKorisnika uloga = prodavac.Uloga;
            bool obrisan = prodavac.Obrisan;
            
            string upisi = id + ";" + korisnickoIme + ";" + lozinka + ";" + ime + ";" + prezime + ";" + pol + ";" + datumRodjenja + ";" + uloga + ";" + "0" + ";" + obrisan ;
            sw.WriteLine(upisi);


            sw.Close();
            stream.Close();
        }
        public static void SacuvajAdministratore(Korisnik administrator)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Administratori.txt");
            FileStream stream = new FileStream(path, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            int id = administrator.Id;
            string korisnickoIme = administrator.KorisnickoIme;
            string lozinka = administrator.Lozinka;
            string ime = administrator.Ime;
            string prezime = administrator.Prezime;
            PolTip pol = administrator.Pol;
            DateTime datumRodjenja = administrator.DatumRodjenja;
            UlogaKorisnika uloga = administrator.Uloga;
            
            string upisi = id + ";" + korisnickoIme + ";" + lozinka + ";" + ime + ";" + prezime + ";" + pol + ";" + datumRodjenja + ";" + uloga + ";" + "0";
            sw.WriteLine(upisi);


            sw.Close();
            stream.Close();
        }
        public static void SacuvajRezervaciju(Rezervacija rezervacija)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Rezervacije.txt");
            FileStream stream = new FileStream(path, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            Karta karta = rezervacija.karta;
            string idKarte = karta.IdKarte;
            double cijenaKarte = karta.Cijena;
            Korisnik kupac = karta.kupac;
            StatusKarte statusKarte = karta.Status;
            TipKarte tipKarte = karta.Tip;

            Manifestacija manifestacija = karta.ZaKojuManifestaciju;

            int id = manifestacija.Id;
            string naziv = manifestacija.Naziv;
            TipManifestacije tip = manifestacija.Tip;
            int brojMjestaRegular = manifestacija.BrojMijestaRegular;
            int brojMjestaVip = manifestacija.BrojMijestaVip;
            int brojMjestaFanPit = manifestacija.BrojMijestaFanPit;
            int ukupno = manifestacija.BrojMijesta;
            int brSlobodnihRegular = manifestacija.BrSlobodnihRegular;
            int brSlobodnihVip = manifestacija.BrSlobodnihVip;
            int brSlobodnihFanPit = manifestacija.BrSlobodnihFanPit;
            DateTime datumiVrijeme = manifestacija.DatumIVrijemeOdrzavanja;
            double cijena = manifestacija.CijenaRegularKarte;
            StatusManifestacije status = manifestacija.Status;
            MjestoOdrzavanja mjesto = manifestacija.Mjesto;
            bool zavrsena = manifestacija.Zavrsena;
            int idProdavca = manifestacija.IdProdavca;
            string img = manifestacija.Poster;
            double prosjecnaOcjena = manifestacija.ProsjecnaOcjena;

            int idKupca = kupac.Id;
            string korisnickoIme = kupac.KorisnickoIme;
            string lozinka = kupac.Lozinka;
            string ime = kupac.Ime;
            string prezime = kupac.Prezime;
            PolTip pol = kupac.Pol;
            DateTime datumRodjenja = kupac.DatumRodjenja;
            UlogaKorisnika uloga = UlogaKorisnika.KUPAC;
            double BrojBodova = kupac.BrojSakupljenihBodova;
            TipKorisnika tipKupca = kupac.Tip;
            bool obrisan = kupac.Obrisan;

            int kolicina = rezervacija.kolicina;
            int idRez = rezervacija.id;

            string upisi = idKarte + ";" + id + ";" + naziv + ";" + tip + ";" + brojMjestaRegular + ";" + brojMjestaVip + ";" + brojMjestaFanPit + ";" + ukupno + ";" + datumiVrijeme + ";" + cijena + ";" + status + ";"+img+ ";" + mjesto.Adresa + ";" + zavrsena + ";" + idProdavca + ";" + cijenaKarte + ";" + idKupca + ";" + korisnickoIme + ";" + lozinka + ";" + ime + ";" + prezime + ";" + pol + ";" + datumRodjenja + ";" + uloga + ";" + BrojBodova + ";" + tipKupca.Ime + ";" + tipKupca.Popust + ";" + tipKupca.BrojBodova + ";" + obrisan + ";" + statusKarte + ";" + tipKarte + ";" + kolicina + ";"+ idRez + ";" + prosjecnaOcjena + ";" + brSlobodnihRegular + ";" + brSlobodnihVip + ";" + brSlobodnihFanPit;
            sw.WriteLine(upisi);


            sw.Close();
            stream.Close();
        }
        public static void SacuvajKomentar(Komentar komentar)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Komentari.txt");
            FileStream stream = new FileStream(path, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            int id = komentar.Id;
            Korisnik k = komentar.KupacKarte;
            int idKupca = k.Id;
            Manifestacija m = komentar.Manifestacija;
            int idManifestacije = m.Id;
            string kom = komentar.TekstKomentara;
            int ocjena = komentar.Ocjena;
            TipKomentara tip = komentar.Tip;

            string upisi = id + ";" + idKupca+ ";" + idManifestacije + ";" + kom + ";" + ocjena + ";" + tip;
            sw.WriteLine(upisi);


            sw.Close();
            stream.Close();
        }

        public static void AzururajKupca(List<Korisnik> kupci)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Kupci.txt");
            File.WriteAllText(path, String.Empty);

            foreach (Korisnik k in kupci)
            {
                SacuvajKupce(k);
            }
        }
        public static void AzururajAdministratora(List<Korisnik> administratori)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Administratori.txt");
            File.WriteAllText(path, String.Empty);

            foreach (Korisnik k in administratori)
            {
                SacuvajAdministratore(k);
            }
        }
        public static void AzururajProdavca(List<Korisnik> prodavci)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Prodavci.txt");
            File.WriteAllText(path, String.Empty);

            foreach (Korisnik k in prodavci)
            {
                SacuvajProdavce(k);
            }
        }
        public static void AzurirajManifestacije(List<Manifestacija> manifestacije)
        {

            string path = HostingEnvironment.MapPath("~/App_Data/Manifestacije.txt");
            File.WriteAllText(path, String.Empty);

            foreach (Manifestacija m in manifestacije)
            {
                SacuvajManifestacije(m);
            }

        }
        public static void AzurirajRezervaciju(List<Rezervacija> rezervacije)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Rezervacije.txt");
            File.WriteAllText(path, String.Empty);

            foreach (Rezervacija rez in rezervacije)
            {
                SacuvajRezervaciju(rez);
            }
        }
        public static void AzurirajKomentare(List<Komentar> komentari)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/Komentari.txt");
            File.WriteAllText(path, String.Empty);

            foreach (Komentar k in komentari)
            {
                SacuvajKomentar(k);
            }
        }
        
       }
}