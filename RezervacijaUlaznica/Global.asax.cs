﻿using RezervacijaUlaznica.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace RezervacijaUlaznica
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Učitamo prodatke iz datoteka u memoriju
            List<Manifestacija> manifestacije = Podaci.ProcitajManifestacije("~/App_Data/Manifestacije.txt");
            HttpContext.Current.Application["manifestacije"] = manifestacije;


            List<Korisnik> administratori = Podaci.ProcitajAdministratore("~/App_Data/Administratori.txt");
            HttpContext.Current.Application["admin"] = administratori;
            
            List<Korisnik> kupci = Podaci.ProcitajKupce("~/App_Data/Kupci.txt"); 
            HttpContext.Current.Application["kupci"] = kupci;

            List<Korisnik> prodavci = Podaci.ProcitajProdavce("~/App_Data/Prodavci.txt");
            HttpContext.Current.Application["prodavci"] = prodavci;

            //string path = HostingEnvironment.MapPath("~/App_Data/Kupovine.txt");
            //File.WriteAllText(path, String.Empty);

            List<Rezervacija> rezervacije = Podaci.ProcitajRezervacije("~/App_Data/Rezervacije.txt");
            HttpContext.Current.Application["rezervacije"] = rezervacije;

            List<Komentar> komentari = Podaci.ProcitajKomentare("~/App_Data/Komentari.txt");
            HttpContext.Current.Application["komentari"] = komentari;



        }
    }
}
